# KlarnaDisputesApiApi\DisputesApi

All URIs are relative to http://localhost.

Method | HTTP request | Description
------------- | ------------- | -------------
[**acceptLoss()**](DisputesApi.md#acceptLoss) | **POST** /v1/disputes/{dispute_id}/accept-loss | Accept dispute loss
[**downloadRequestAttachment()**](DisputesApi.md#downloadRequestAttachment) | **GET** /v1/disputes/{dispute_id}/requests/{request_id}/attachments/{attachment_id} | Download request attachment
[**downloadResponseAttachment()**](DisputesApi.md#downloadResponseAttachment) | **GET** /v1/disputes/{dispute_id}/requests/{request_id}/responses/{response_id}/attachments/{attachment_id} | Download response attachment
[**getDisputeDetails()**](DisputesApi.md#getDisputeDetails) | **GET** /v1/disputes/{dispute_id} | Get Dispute Details
[**listDisputes()**](DisputesApi.md#listDisputes) | **GET** /v1/disputes | List Disputes
[**respondToDisputeRequest()**](DisputesApi.md#respondToDisputeRequest) | **POST** /v1/disputes/{dispute_id}/requests/{request_id}/responses | Respond to Dispute Request
[**uploadAttachment()**](DisputesApi.md#uploadAttachment) | **POST** /v1/disputes/{dispute_id}/attachments | Upload attachment


## `acceptLoss()`

```php
acceptLoss($dispute_id)
```

Accept dispute loss

Accept the loss of a dispute, which will be closed with the chargeback activation

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute to accept loss for

try {
    $apiInstance->acceptLoss($dispute_id);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->acceptLoss: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute to accept loss for |

### Return type

void (empty response body)

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadRequestAttachment()`

```php
downloadRequestAttachment($dispute_id, $request_id, $attachment_id): \SplFileObject
```

Download request attachment

Download a file attachment linked to a specific request.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute for which the attachment to fetch belongs to
$request_id = 56; // int | ID of request for which the attachment to fetch belongs to
$attachment_id = 56; // int | ID of the attachment to fetch

try {
    $result = $apiInstance->downloadRequestAttachment($dispute_id, $request_id, $attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->downloadRequestAttachment: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute for which the attachment to fetch belongs to |
 **request_id** | **int**| ID of request for which the attachment to fetch belongs to |
 **attachment_id** | **int**| ID of the attachment to fetch |

### Return type

**\SplFileObject**

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/octet-stream`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadResponseAttachment()`

```php
downloadResponseAttachment($dispute_id, $request_id, $response_id, $attachment_id): \SplFileObject
```

Download response attachment

Download a file attachment linked to a specific request response.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute for which the attachment to fetch belongs to
$request_id = 56; // int | ID of request for which the attachment to fetch belongs to
$response_id = 56; // int | ID of response for which the attachment to fetch belongs to
$attachment_id = 56; // int | ID of the attachment to fetch

try {
    $result = $apiInstance->downloadResponseAttachment($dispute_id, $request_id, $response_id, $attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->downloadResponseAttachment: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute for which the attachment to fetch belongs to |
 **request_id** | **int**| ID of request for which the attachment to fetch belongs to |
 **response_id** | **int**| ID of response for which the attachment to fetch belongs to |
 **attachment_id** | **int**| ID of the attachment to fetch |

### Return type

**\SplFileObject**

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/octet-stream`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getDisputeDetails()`

```php
getDisputeDetails($dispute_id): \KlarnaDisputesApiApi\Model\DisputesApiDisputeDetails
```

Get Dispute Details

Fetch a fully detailed version of a Dispute, including all the associated requests and responses.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute to fetch

try {
    $result = $apiInstance->getDisputeDetails($dispute_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->getDisputeDetails: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute to fetch |

### Return type

[**\KlarnaDisputesApiApi\Model\DisputesApiDisputeDetails**](../Model/DisputesApiDisputeDetails.md)

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `listDisputes()`

```php
listDisputes($merchant_id, $order_id, $dispute_id, $investigation_status, $reason, $opened_after, $opened_before, $deadline_expires_after, $deadline_expires_before, $sort_by, $offset, $size): \KlarnaDisputesApiApi\Model\DisputesApiDisputesList
```

List Disputes

Retrieve a list of Disputes, segregated by a variety of filter parameters

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$merchant_id = array('merchant_id_example'); // string[] | Merchant ID to filter by
$order_id = array('order_id_example'); // string[] | Order ID to filter by
$dispute_id = array('dispute_id_example'); // string[] | Dispute ID to filter by
$investigation_status = array('investigation_status_example'); // string[] | Investigation status of disputes to filter by
$reason = array('reason_example'); // string[] | Dispute reason to filter by
$opened_after = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | Start of dispute opened date interval to filter by
$opened_before = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | End of dispute opened date interval to filter by
$deadline_expires_after = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | Start of dispute deadline expiration interval to filter by
$deadline_expires_before = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | End of dispute deadline expiration interval to filter by
$sort_by = array('sort_by_example'); // string[] | Specifies in which order to sort the results. Then results are ordered in ascending order if not prefixed by a - which orders by descending order.
$offset = 56; // int | maximum number of results to return
$size = 56; // int | How many elements to include in the result

try {
    $result = $apiInstance->listDisputes($merchant_id, $order_id, $dispute_id, $investigation_status, $reason, $opened_after, $opened_before, $deadline_expires_after, $deadline_expires_before, $sort_by, $offset, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->listDisputes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | [**string[]**](../Model/string.md)| Merchant ID to filter by | [optional]
 **order_id** | [**string[]**](../Model/string.md)| Order ID to filter by | [optional]
 **dispute_id** | [**string[]**](../Model/string.md)| Dispute ID to filter by | [optional]
 **investigation_status** | [**string[]**](../Model/string.md)| Investigation status of disputes to filter by | [optional]
 **reason** | [**string[]**](../Model/string.md)| Dispute reason to filter by | [optional]
 **opened_after** | **\DateTime**| Start of dispute opened date interval to filter by | [optional]
 **opened_before** | **\DateTime**| End of dispute opened date interval to filter by | [optional]
 **deadline_expires_after** | **\DateTime**| Start of dispute deadline expiration interval to filter by | [optional]
 **deadline_expires_before** | **\DateTime**| End of dispute deadline expiration interval to filter by | [optional]
 **sort_by** | [**string[]**](../Model/string.md)| Specifies in which order to sort the results. Then results are ordered in ascending order if not prefixed by a - which orders by descending order. | [optional]
 **offset** | **int**| maximum number of results to return | [optional]
 **size** | **int**| How many elements to include in the result | [optional]

### Return type

[**\KlarnaDisputesApiApi\Model\DisputesApiDisputesList**](../Model/DisputesApiDisputesList.md)

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `respondToDisputeRequest()`

```php
respondToDisputeRequest($dispute_id, $request_id, $disputes_api_new_response): \KlarnaDisputesApiApi\Model\DisputesApiResponse
```

Respond to Dispute Request

Respond to a Dispute request.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute for which the request to add response to belongs
$request_id = 56; // int | ID of request to add response to
$disputes_api_new_response = new \KlarnaDisputesApiApi\Model\DisputesApiNewResponse(); // \KlarnaDisputesApiApi\Model\DisputesApiNewResponse | Response to add to request

try {
    $result = $apiInstance->respondToDisputeRequest($dispute_id, $request_id, $disputes_api_new_response);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->respondToDisputeRequest: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute for which the request to add response to belongs |
 **request_id** | **int**| ID of request to add response to |
 **disputes_api_new_response** | [**\KlarnaDisputesApiApi\Model\DisputesApiNewResponse**](../Model/DisputesApiNewResponse.md)| Response to add to request |

### Return type

[**\KlarnaDisputesApiApi\Model\DisputesApiResponse**](../Model/DisputesApiResponse.md)

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `uploadAttachment()`

```php
uploadAttachment($dispute_id, $filename, $file): \KlarnaDisputesApiApi\Model\DisputesApiAttachment
```

Upload attachment

Uploads an attachment to be linked to a dispute reponse

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute for which the request to add response to belongs
$filename = 'filename_example'; // string | Name of a file to upload
$file = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->uploadAttachment($dispute_id, $filename, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->uploadAttachment: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dispute_id** | **string**| ID of dispute for which the request to add response to belongs |
 **filename** | **string**| Name of a file to upload |
 **file** | **\SplFileObject****\SplFileObject**|  |

### Return type

[**\KlarnaDisputesApiApi\Model\DisputesApiAttachment**](../Model/DisputesApiAttachment.md)

### Authorization

[apiUser](../../README.md#apiUser)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
