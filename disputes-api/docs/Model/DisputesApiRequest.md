# # DisputesApiRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**responses** | [**\KlarnaDisputesApiApi\Model\DisputesApiResponse[]**](DisputesApiResponse.md) |  | [optional]
**attachments** | [**\KlarnaDisputesApiApi\Model\DisputesApiAttachment[]**](DisputesApiAttachment.md) |  |
**comment** | **string** | Customer service comment regarding the associated request |
**created_at** | **\DateTime** | Creation date of the customer service request |
**optional_requested_fields** | **string[]** | Data associated with the current dispute required by customer service merchants should respond with |
**request_id** | **int** | Unique identifier of the request associated with the current dispute |
**requested_fields** | **string[]** | Data associated with the current dispute required by customer service merchants should respond with |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
