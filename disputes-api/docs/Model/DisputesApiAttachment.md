# # DisputesApiAttachment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachment_id** | **int** | Incremental identifier of an attachment associated with a merchant response |
**filename** | **string** | Name of a file attached to a merchant response |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
