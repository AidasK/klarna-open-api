# # DisputesApiMerchant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_id** | **string** | Unique merchant identifier associated with the current dispute |
**name** | **string** | Merchant company name |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
