# # DisputesApiPagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Number of elements returned |
**offset** | **int** | Number of elements to skip |
**total** | **int** | Total number of elements |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
