{
  "openapi": "3.0.0",
  "info": {
    "title": "consumer-banking-xs2a",
    "description": "Our Access to Account (XS2A) API is the PSD2 compliant interface for accessing account information and initiating payments on behalf of Klarna payment account users.\n\nIf you are looking for the documentation of the **Open banking. by Klarna** solution, you can find the docs [`here`](https://docs.openbanking.klarna.com/index.html).\n\nYou can find more information about PSD2 [`here`](https://www.klarna.com/pay-now/customer-service/what-is-psd2/).",
    "version": "1.0.0",
    "x-document-metadata": {
      "copyright": "© 2005 - 2021 Klarna Bank AB (publ). All rights reserved.",
      "trace-id": "1c188-1c188-e8f4b-klarna-api-production-s3-20211217094500",
      "date-created": "2021-12-17T09:45:00Z"
    }
  },
  "tags": [
    {
      "name": "consents",
      "description": "Endpoints for creating and fetching consent sessions."
    },
    {
      "name": "accounts",
      "description": "Endpoints for getting account details."
    },
    {
      "name": "payments",
      "description": "Endpoints for initiating and fetching payments."
    },
    {
      "name": "Requirements",
      "description": "To access Klarna’s XS2A API in production you must be a licensed TPP according to PSD2 legislation and be in possession of an [`eIDAS certificate`](https://eba.europa.eu/sites/default/documents/files/documents/10180/2137845/d429d45e-f936-473c-bc02-c23060d11f19/EBA%20Opinion%20on%20the%20use%20of%20eIDAS%20certificates%20under%20the%20RTS%20on%20SCACSC.pdf) for the purpose of mutual identification.",
      "x-traitTag": true
    },
    {
      "name": "Authentication",
      "description": "All API endpoints are protected using mutual TLS and require a valid eIDAS certificate, which are issued by one of the approved Trust Centers. In Germany, these test certificates are issued, among others, by the [`Bundesdruckerei`](https://www.bundesdruckerei.de/de/loesungen/PSD2).\nThe certificate is being validated and the given TPP roles (AIS, PIS or PIIS) are extracted. If the certificate is invalid or lacks the required permissions level for the given request, the API will respond with a 403 error.",
      "x-traitTag": true
    },
    {
      "name": "URLs",
      "description": "**Production: [`https://xs2a.banking.klarna.com`](https://xs2a.banking.klarna.com/)**\n\n**Sandbox: [`https://xs2a.banking.playground.klarna.com`](https://xs2a.banking.playground.klarna.com/)**\n\nTo use our sandbox environment, you can use your production certificate or, if you're not yet in possession of such, you can use our test certificate to authenticate yourself in playground.\nTest certificate:\ncrt file:\n\n```\n-----BEGIN CERTIFICATE-----\nMIIFEzCCAvsCCQCfm/cuajYlPDANBgkqhkiG9w0BAQsFADCB0zELMAkGA1UEBhMC\nREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGluMQ8wDQYDVQQKDAZL\nbGFybmExHTAbBgNVBAsMFEJhbmtpbmcgSW50ZWdyYXRpb25zMUEwPwYDVQQDDDhL\nbGFybmEgQmFua2luZyBJbnRlZ3JhdGlvbnMgWFMyQSBQbGF5Z3JvdW5kIFRlc3Qg\nUm9vdCBDQTEvMC0GCSqGSIb3DQEJARYgY29uc3VtZXIuYmFua2luZy54czJhQGts\nYXJuYS5jb20wHhcNMjEwMjA0MDk0NTIzWhcNMjIwNjE5MDk0NTIzWjCBwjELMAkG\nA1UEBhMCREUxDzANBgNVBAgMBkJlcmxpbjEPMA0GA1UEBwwGQmVybGluMQ8wDQYD\nVQQKDAZLbGFybmExHTAbBgNVBAsMFEJhbmtpbmcgSW50ZWdyYXRpb25zMTAwLgYD\nVQQDDCdLbGFybmEgWFMyQSBQbGF5Z3JvdW5kIHRlc3QgY2VydGlmaWNhdGUxLzAt\nBgkqhkiG9w0BCQEWIGNvbnN1bWVyLmJhbmtpbmcueHMyYUBrbGFybmEuY29tMIIB\nIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq25ODOSta525Gwn/xEb202pI\nAdJlV4HjBWNSZAcHU19ZdnqeOp+dkDpzAC2TjKGqaKlLip5FT5Yks+/ittp30DHZ\nE1LQC6N9Q6+ebj7shqJiU5ztyVN1uPm437YzCkMkPgnLGcVNBqFVxdmtABHelp4B\nEjmVyIh4Ir68mAqzIBZB3H3+msaOZNo4gwRfAiq/EF6M4ZoaN5MhBtmP+TpF4NKG\n+K8+tPSHpV1FIepDbpiuIVddEtGoTMTCkZ0lDKVeNa1mPG2zOs/Nrgfe0UzFVWCA\n9+3sydq7BJFm3Jz1UcUUqAN4fiHkq2xW2JuAuppqp+/udacxYzX+XN/wC/iXowID\nAQABMA0GCSqGSIb3DQEBCwUAA4ICAQCEpvvDnR0L/i9RFZU1kkHlAOy/KaPpFhKm\nllxUjel+0t1sNODdEUHWYXW1R2vPub/e4sc+SsxIdo/xt+lMFv+w5gNC87AjiC2G\n6ENB+deNkNdsbcF26+ehJLeaL4udoVcw3gJh9uPa8zLAin2ZhejCjIsbrdpg85P/\nFLIkI45uyHH46LmtWrGadTTezsV0OELewbX6jvTbn8Kkno2M2JiSUYZHFYm3M+r/\ngnWLGBzBXFPRuDVNsDkvAiOR191+5j3m4rwU0OBOqoMzPg7dzbQWk9MmJbGulGYk\npToU+gdBTRc9q4QU0gwn6b3JWzxMm8WoohQIL6AT5ZeiXHeBwBH0JHK26dLX2TmH\n/Fn5eZB7NGYvGmB1BGZiffgHlU+SsHb5zAn3JzG/hMatTemrfg45BpHswAHtz278\nURYZ+ylGCUgFaNwL7XSv6pdoRWeYmRz/NJvKsx1khbcoMWK/pwHlRMbAy0YdNcSc\n/zXUA/TGpchPHvEg9O/H3/gpo3dDnf9SigANFfKQt61am3rTVzGCILXuORNLRGWX\n9UReoSCjSBMCN6r6VFHUl2Jly1Hlwd4fIU97ybDNvO/sbXai6UY3FY/b1M8o1elj\nbTyhkcUQEt/PBiG4m/aIcxDy/fL2/c1g5q/iGjvXUm5MsPTVnIKwpIJDf62vpSn6\nlK1Lum8wzg==\n-----END CERTIFICATE-----\n```\n\nkey file:\n\n```\n-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAq25ODOSta525Gwn/xEb202pIAdJlV4HjBWNSZAcHU19Zdnqe\nOp+dkDpzAC2TjKGqaKlLip5FT5Yks+/ittp30DHZE1LQC6N9Q6+ebj7shqJiU5zt\nyVN1uPm437YzCkMkPgnLGcVNBqFVxdmtABHelp4BEjmVyIh4Ir68mAqzIBZB3H3+\nmsaOZNo4gwRfAiq/EF6M4ZoaN5MhBtmP+TpF4NKG+K8+tPSHpV1FIepDbpiuIVdd\nEtGoTMTCkZ0lDKVeNa1mPG2zOs/Nrgfe0UzFVWCA9+3sydq7BJFm3Jz1UcUUqAN4\nfiHkq2xW2JuAuppqp+/udacxYzX+XN/wC/iXowIDAQABAoIBAQCVj25YoUkLEldj\n151kR+UfHSIlNMFlTtQY50YCLn+dpRoP23of+xEju1qJVTnc/04EcW6OSPZ/MKZk\ntWw9yuusn1CbIyyOetvWKWk3FX6VFyZhfA+YDVAvSO6ZfgOZeGouFa0zloZUunxy\nWC4uWWiwTrjwhXy/rw/ggDdZDjyoSFUEadIMYMy6ehwZ4lzm3MnJle2aK/2HnebT\nLzl8F0p4jjyYjwR6VtzAPJhTs707C8u7e8Y2y2k9gpQOoRAygOLUZt2xh785Oefa\nuTSA5VCoq95O/WPuZr4rXulwQ5LEfA4HNkrjX/I44AZiPjwlhpbpi78yHtBiL85G\nKbS+kh7BAoGBANhRm3bUOZ5och99fQ85WrkzsXONM/Flvckp1+YaqtEQkuyjzHhG\nPnOvl0w+Ry2Z6jfmUw9vJYwKJna4I51L06PN/4/w7WGTDC1VC2Lb+ewbfTS+e6XM\ndpEfLzJF3Qc9S+bl9/Wpw6ZkaoWkrusGhtOJ8CEqMLvy5lzZ2C7Othr9AoGBAMrg\nv69mU80DAKh+2utOVs7Hgh6YO8lhj4CxPn/f2K1fais5ylLozdSHOqsyK5+9gHm1\nTkxWni0y1z3pEajn5KcTI1m/7NkMMjs1jmpKPiyDzGnxCgfAfJJoNV/BNXmhpGa7\nU9IXDQQacBugAYKqpNFATVjkT/EeNml/6gMJuI8fAoGBALiV8nYUpFIix/dNCpEa\nHOtaS/rRK9i6O5dTaFKGwOoDHYxmPU0Ii7QFyrIZln04EF/A33GVI1H7O1Ukzeck\nVCU72+6E5NrVMpfgMQYbtYie8Fk4jaQt86LkHE7mxLOvv3v5EnyDb1sl5qvBmi0c\n9aG+27RBHzLSE3Z5wXo3k7c1AoGAQ0rtCdbdMQEv9LOvFaG0dX4DkPZZzmBH4x3n\nZy7uhgNCCfhvPbTuoPIB6csIYUHQHY6f2/5XFRLqBiUKsIXKeAiHDzvz6cJ73tvS\n0zqxCL7mzLUg6JVWPJHIMmy5uhB9oX29PdBbhuLmEKOwHJFRff7gKP4B7i5hSfkd\nu8g7qW0CgYBarpUWK3sXtK+ZiOtA7GDTL5xWKzWU2pJDKLwfdoERkBKhupBYiDq3\nu/2qrQmzoUYKtSEd6eE/eEpSJYUOReFa4SV0VGn/xUxaoiqLqhAOd4WkP4617ump\n88MgWjnnmCRL+GN0BvHLFo3RBfGu8z84EU8vhc4qJyW5mcWKXfJANQ==\n-----END RSA PRIVATE KEY-----\n```",
      "x-traitTag": true
    },
    {
      "name": "Support",
      "description": "If you need support you can email us at: [`support.xs2a.banking@klarna.com`](mailto:support.xs2a.banking@klarna.com)",
      "x-traitTag": true
    }
  ],
  "paths": {
    "/v2/consent-sessions": {
      "post": {
        "tags": [
          "consents"
        ],
        "summary": "Create consent session.",
        "description": "Create a consent session by providing email and permissions.",
        "operationId": "consentFlow",
        "parameters": [
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/post_consent_session_request_body"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "Consent session response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/post_consent_session_response"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "403": {
            "description": "The client does not have access rights to the content. This could be caused by many things - an invalid certificate, missing permission or an attempted access to resources that the TPP has no access to. The error message and code should give more context for the cause of the error."
          },
          "404": {
            "description": "Could not initiate consent session."
          }
        }
      }
    },
    "/v2/consent-sessions/{consent_session_id}": {
      "get": {
        "summary": "Get consent session.",
        "description": "Get the status of a consent session.",
        "operationId": "getConsentFlow",
        "tags": [
          "consents"
        ],
        "parameters": [
          {
            "description": "ID of the consent session.",
            "in": "path",
            "name": "consent_session_id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "responses": {
          "200": {
            "description": "Consent session response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/get_consent_session_response"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not get consent session."
          }
        }
      }
    },
    "/v2/accounts": {
      "get": {
        "summary": "Get account list.",
        "description": "Get the list of accounts of a given user.",
        "operationId": "accountsFlow",
        "tags": [
          "accounts"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/consent_header"
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "responses": {
          "200": {
            "description": "Accounts response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/accounts"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not get accounts."
          }
        }
      }
    },
    "/v2/accounts/{account_id}": {
      "get": {
        "summary": "Get account details.",
        "description": "Get the account details of a given account.",
        "operationId": "accountFlow",
        "tags": [
          "accounts"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/consent_header"
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          },
          {
            "name": "account_id",
            "in": "path",
            "description": "account id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Account response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/account"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not get account."
          }
        }
      }
    },
    "/v2/accounts/{account_id}/balance": {
      "get": {
        "summary": "Get account balance.",
        "description": "Get the account balance for a given account.",
        "operationId": "accountBalanceFlow",
        "tags": [
          "accounts"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/consent_header"
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          },
          {
            "name": "account_id",
            "in": "path",
            "description": "account id",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Account balance response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/balance"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not get account balance."
          }
        }
      }
    },
    "/v2/accounts/{account_id}/transactions": {
      "get": {
        "summary": "Get paginated transaction list.",
        "description": "Get the transactions for a given account.",
        "operationId": "accountTransactionsFlow",
        "tags": [
          "accounts"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/consent_header"
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          },
          {
            "name": "account_id",
            "in": "path",
            "description": "account id",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "start_at",
            "in": "query",
            "description": "Format: `yyyy-MM-dd` Earliest inclusive initiation or completion date of a transaction.",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "end_at",
            "in": "query",
            "description": "Format: `yyyy-MM-dd` Latest inclusive initiation or completion date of a transaction.",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "page_number",
            "in": "query",
            "description": "Page number - defaults to 0",
            "required": false,
            "schema": {
              "type": "integer",
              "minimum": 0,
              "default": 0
            }
          },
          {
            "name": "page_size",
            "in": "query",
            "description": "Page size - defaults to 20",
            "required": false,
            "schema": {
              "type": "integer",
              "minimum": 1,
              "maximum": 100,
              "default": 20
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Paginated transactions response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/transactions_response"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not get account transactions."
          }
        }
      }
    },
    "/v2/payments": {
      "post": {
        "summary": "Create payment.",
        "description": "Initiate a standard or standing order payment.",
        "operationId": "paymentFlow",
        "tags": [
          "payments"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/post_payment_request_body"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Initiated payment response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/post_payment_response"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not initiate payment."
          }
        }
      }
    },
    "/v2/payments/{payment_id}": {
      "get": {
        "summary": "Get payment details.",
        "description": "Get the payment details of a given payment.",
        "operationId": "getPaymentFlow",
        "tags": [
          "payments"
        ],
        "parameters": [
          {
            "name": "payment_id",
            "in": "path",
            "description": "ID of the payment.",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "responses": {
          "200": {
            "description": "Payment response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/get_payment_response"
                }
              }
            }
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not find payment."
          }
        }
      },
      "delete": {
        "summary": "Revoke a payment",
        "description": "This will only revoke payments with status AWAITING_APPROVAL",
        "operationId": "deletePaymentFlow",
        "tags": [
          "payments"
        ],
        "parameters": [
          {
            "name": "payment_id",
            "in": "path",
            "description": "ID of the payment.",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "400": {
            "description": "Validation of request parameters failed."
          },
          "401": {
            "description": "Authorization information is missing or invalid."
          },
          "403": {
            "description": "The client does not have access rights to the content."
          },
          "404": {
            "description": "Could not find payment."
          }
        }
      }
    },
    "/v2/funds-confirmations": {
      "post": {
        "summary": "Confirm available funds.",
        "description": "Confirm of the available funds for an amount and a given email in a given currency.",
        "operationId": "fundFlow",
        "tags": [
          "accounts"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/consent_header"
          },
          {
            "$ref": "#/components/parameters/x_request_id_header"
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/post_funds_confirmation_request_body"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Fund confirmation response.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/funds_confirmation"
                }
              }
            }
          },
          "400": {
            "description": "The requested account has a different currency."
          },
          "403": {
            "description": "Account not approved for the consent-id."
          },
          "404": {
            "description": "The requested account does not exist."
          }
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about the Klarna REST APIs",
    "url": "https://docs.klarna.com/api"
  },
  "x-tagGroups": [
    {
      "name": "Get Started",
      "tags": [
        "Requirements",
        "Authentication",
        "URLs",
        "Support"
      ]
    },
    {
      "name": "XS2A API",
      "tags": [
        "consents",
        "accounts",
        "payments"
      ]
    }
  ],
  "servers": [
    {
      "url": "https://api.klarna.com"
    }
  ],
  "components": {
    "parameters": {
      "consent_header": {
        "name": "consent-id",
        "in": "header",
        "description": "The consent-id that was received in the GET /consent-sessions/{consent_session_id} request",
        "required": true,
        "schema": {
          "type": "string"
        }
      },
      "x_request_id_header": {
        "name": "X-Request-ID",
        "in": "header",
        "description": "ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.",
        "required": false,
        "schema": {
          "type": "string"
        }
      }
    },
    "schemas": {
      "post_consent_session_request_body": {
        "type": "object",
        "required": [
          "email",
          "postal_code",
          "permission"
        ],
        "properties": {
          "email": {
            "description": "Email address the user used for their Klarna Bank Account.",
            "type": "string",
            "example": "sven.svenson@klarna.com"
          },
          "postal_code": {
            "type": "string",
            "description": "Postal code of the consent session creator.",
            "example": "10115"
          },
          "permission": {
            "description": "Type of PSD2 permission level to grant.",
            "type": "array",
            "items": {
              "type": "string",
              "enum": [
                "AIS",
                "PIIS"
              ]
            },
            "example": [
              "AIS"
            ]
          }
        }
      },
      "post_consent_session_response": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the created consent session.",
            "example": "04d2bd77-f310-4c45-b435-a0bb588697ef"
          },
          "status": {
            "type": "string",
            "enum": [
              "AWAITING_APPROVAL",
              "APPROVED",
              "EXPIRED",
              "REJECTED"
            ],
            "description": "Status of the created consent session.",
            "example": "AWAITING_APPROVAL"
          },
          "consent_id": {
            "type": "string",
            "description": "ID of the created consent once the consent session was approved by the PSU.",
            "example": "null"
          }
        }
      },
      "get_consent_session_response": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the created consent session.",
            "example": "04d2bd77-f310-4c45-b435-a0bb588697ef"
          },
          "status": {
            "type": "string",
            "enum": [
              "AWAITING_APPROVAL",
              "APPROVED",
              "EXPIRED",
              "REJECTED"
            ],
            "description": "Status of the created consent session.",
            "example": "APPROVED"
          },
          "consent_id": {
            "type": "string",
            "description": "ID of the created consent once the consent session was approved by the PSU.",
            "example": "06e11107-c9c1-466a-b686-2223161692d0"
          }
        }
      },
      "account": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the account.",
            "example": "135d9376-b529-44f1-b5a4-bc54f54c9785"
          },
          "type": {
            "type": "string",
            "description": "Type of the account.",
            "example": "CURRENT"
          },
          "account_holder_id": {
            "type": "string",
            "description": "ID of the account holder.",
            "example": "c11ee70f-0769-4a73-9c7c-1b000b3d8756"
          },
          "account_number": {
            "type": "string",
            "description": "Account number.",
            "example": "null"
          },
          "iban": {
            "type": "string",
            "description": "IBAN number of the account.",
            "example": "DE2635638985986393786474"
          },
          "bic": {
            "type": "string",
            "description": "BIC number of the account.",
            "example": "KLRNDEBE"
          },
          "holder_name": {
            "type": "string",
            "description": "Name or information of the Klarna Bank Account owner.",
            "example": "Sven Svenson"
          },
          "holder_address": {
            "type": "string",
            "description": "Address of the Klarna Bank Account owner.",
            "example": "Chausseestraße 117, Berlin"
          },
          "bank_name": {
            "type": "string",
            "description": "Name or information of the bank.",
            "example": "Bank Name"
          },
          "bank_address": {
            "type": "string",
            "description": "Address of the bank.",
            "example": "Chausseestraße 117, Berlin"
          },
          "sort_code": {
            "$ref": "#/components/schemas/sort_code"
          },
          "balance": {
            "$ref": "#/components/schemas/balance"
          }
        }
      },
      "accounts": {
        "type": "object",
        "properties": {
          "accounts": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/account"
            }
          }
        }
      },
      "balance": {
        "type": "object",
        "description": "Account balance.",
        "properties": {
          "amount": {
            "type": "number",
            "description": "Balance amount in the smallest unit for the given currency. For EUR, it returns the balance in cents.",
            "example": 4000
          },
          "currency": {
            "type": "string",
            "description": "Account currency.",
            "example": "EUR"
          }
        }
      },
      "transaction_details": {
        "type": "object",
        "description": "Transaction details",
        "properties": {
          "type": {
            "type": "string",
            "description": "Transaction type",
            "enum": [
              "CARD_TRANSACTION",
              "INCOMING_TRANSACTION",
              "OUTGOING_TRANSACTION"
            ],
            "example": "OUTGOING_TRANSACTION"
          },
          "iban": {
            "type": "string",
            "description": "IBAN of the other party bank",
            "example": "DE89370400440532013000"
          },
          "bic": {
            "type": "string",
            "description": "BIC of the other party bank",
            "example": "NTSBDEB1XXX"
          },
          "is_atm_withdrawal": {
            "type": "boolean",
            "example": false
          },
          "merchant_amount": {
            "$ref": "#/components/schemas/transaction_amount"
          },
          "merchant_name": {
            "type": "string",
            "description": "Name of the merchant - only for type CARD_TRANSACTION",
            "example": "Zara Fashion"
          },
          "merchant_country": {
            "type": "string",
            "description": "Country of the merchant - only for type CARD_TRANSACTION",
            "example": "DEU"
          },
          "merchant_city": {
            "type": "string",
            "description": "City of the merchant - only for type CARD_TRANSACTION",
            "example": "Berlin"
          },
          "merchant_category_code": {
            "type": "string",
            "description": "Category code of the payment - only for type CARD_TRANSACTION",
            "example": "5411"
          },
          "foreign_exchange_rate": {
            "type": "number",
            "description": "In case the merchant amount is different from the transaction amount the rate will be != 1 - only for type CARD_TRANSACTION",
            "example": 1
          },
          "sepa_transaction_type": {
            "type": "string",
            "description": "SEPA type of the transaction",
            "example": "DIRECT_DEBIT"
          }
        },
        "required": [
          "type"
        ]
      },
      "transaction_amount": {
        "type": "object",
        "description": "Amount object",
        "properties": {
          "amount": {
            "type": "number",
            "description": "Amount",
            "example": 4000
          },
          "currency": {
            "type": "string",
            "description": "Currency",
            "example": "EUR"
          }
        }
      },
      "transaction": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the transaction",
            "example": "3d20a6d3-1d21-4560-bb3b-cf9f4912d65c"
          },
          "amount": {
            "$ref": "#/components/schemas/transaction_amount"
          },
          "balance_after_transaction": {
            "$ref": "#/components/schemas/transaction_amount"
          },
          "created_at": {
            "type": "string",
            "description": "Created at date of the transaction",
            "example": "2020-09-15T08:03:10.273Z"
          },
          "status": {
            "type": "string",
            "description": "Status of the transaction",
            "enum": [
              "COMPLETED",
              "FAILED",
              "REFUNDED",
              "EXPIRED"
            ],
            "example": "COMPLETED"
          },
          "transaction_details": {
            "$ref": "#/components/schemas/transaction_details"
          },
          "other_party": {
            "type": "string",
            "description": "Other party's name or information",
            "example": "Max Mustermann"
          },
          "reference_text": {
            "type": "string",
            "description": "Reference of the transaction",
            "example": "reference"
          }
        },
        "required": [
          "id",
          "amount",
          "balance_after_transaction",
          "created_at",
          "status",
          "transaction_details"
        ]
      },
      "transactions_response": {
        "type": "object",
        "properties": {
          "account_id": {
            "type": "string",
            "description": "ID of the user's account",
            "example": "66d3b648-f5f4-414e-8a3d-5fb6e1a6d05c"
          },
          "transactions": {
            "type": "array",
            "description": "Array of transactions",
            "items": {
              "$ref": "#/components/schemas/transaction"
            }
          },
          "page_info": {
            "$ref": "#/components/schemas/page_info"
          }
        }
      },
      "post_payment_request_body": {
        "type": "object",
        "required": [
          "amount",
          "currency",
          "email",
          "postal_code",
          "to"
        ],
        "properties": {
          "amount": {
            "type": "number",
            "description": "Payment amount.",
            "example": 5000
          },
          "currency": {
            "type": "string",
            "description": "Payment currency.",
            "example": "EUR"
          },
          "email": {
            "type": "string",
            "description": "Email of the payment creator.",
            "example": "sven.svenson@klarna.com"
          },
          "postal_code": {
            "type": "string",
            "description": "Postal code of the payment creator.",
            "example": "10115"
          },
          "reference": {
            "type": "string",
            "description": "Payment reference.",
            "example": "Reference"
          },
          "to": {
            "type": "object",
            "required": [
              "iban",
              "bic",
              "recipient_name"
            ],
            "properties": {
              "iban": {
                "type": "string",
                "description": "IBAN of the other party bank.",
                "example": "SE2635638985986393786474"
              },
              "bic": {
                "type": "string",
                "description": "BIC of the other party bank.",
                "example": "KLRNSESSXXX"
              },
              "recipient_name": {
                "type": "string",
                "description": "Other party's name or information.",
                "example": "Sven Svenson"
              }
            }
          },
          "standing_order": {
            "type": "object",
            "required": [
              "start_date",
              "frequency"
            ],
            "properties": {
              "start_date": {
                "type": "string",
                "description": "Format:yyyy-MM-dd Starting date for standing order payment.",
                "example": "2020-06-12"
              },
              "end_date": {
                "type": "string",
                "description": "Format:yyyy-MM-dd Last date for standing order payment.",
                "example": "2020-10-12"
              },
              "frequency": {
                "type": "string",
                "description": "Frequency of standing order payment.",
                "enum": [
                  "ONCE",
                  "WEEKLY",
                  "MONTHLY",
                  "QUARTERLY",
                  "ANNUALLY"
                ],
                "example": "MONTHLY"
              }
            }
          }
        }
      },
      "post_payment_response": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the initiated payment.",
            "example": "60e5d557-964c-43ab-a19f-0c7d29bd9d84"
          },
          "status": {
            "type": "string",
            "description": "Status of the initiated payment.",
            "example": "AWAITING_APPROVAL"
          }
        }
      },
      "get_payment_response": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "ID of the payment.",
            "example": "60e5d557-964c-43ab-a19f-0c7d29bd9d84"
          },
          "status": {
            "type": "string",
            "description": "Status of the payment.",
            "enum": [
              "APPROVED",
              "EXPIRED",
              "REJECTED",
              "AWAITING_APPROVAL"
            ],
            "example": "APPROVED"
          },
          "sender_details": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string",
                "description": "Name of the sender, or null if payment is not APPROVED.",
                "example": "Sven Svenson"
              },
              "iban": {
                "type": "string",
                "description": "IBAN of the sender's account, or null if payment is not APPROVED.",
                "example": "SE2635638985986393786474"
              }
            }
          }
        }
      },
      "post_funds_confirmation_request_body": {
        "type": "object",
        "required": [
          "amount",
          "currency",
          "email"
        ],
        "properties": {
          "amount": {
            "type": "number",
            "description": "Fund amount in the smallest unit of given currency (e.g. 1.89 EUR would be { amount: 189 }).",
            "example": 300
          },
          "currency": {
            "type": "string",
            "description": "Currency.",
            "example": "EUR"
          },
          "email": {
            "description": "Email address the user used for their Klarna Bank Account.",
            "type": "string",
            "example": "sven.svenson@klarna.com"
          }
        }
      },
      "funds_confirmation": {
        "type": "object",
        "properties": {
          "result": {
            "type": "boolean",
            "description": "Fund confirmation.",
            "enum": [
              true,
              false
            ],
            "example": true
          }
        }
      },
      "sort_code": {
        "type": "string",
        "description": "Klarna sort code.",
        "example": "10010300"
      },
      "page_info": {
        "type": "object",
        "description": "Details of the current transaction pagination",
        "properties": {
          "page_number": {
            "type": "integer",
            "description": "Page number"
          },
          "page_size": {
            "type": "integer",
            "description": "Page size"
          },
          "total_elements": {
            "type": "integer",
            "description": "Number of total transactions"
          },
          "total_pages": {
            "type": "integer",
            "description": "Number of total pages given the page size and total elements"
          }
        }
      }
    }
  }
}