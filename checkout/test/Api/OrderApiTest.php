<?php
/**
 * OrderApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaCheckoutApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Checkout API V3
 *
 * API spec
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace KlarnaCheckoutApi\Test\Api;

use \KlarnaCheckoutApi\Configuration;
use \KlarnaCheckoutApi\ApiException;
use \KlarnaCheckoutApi\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * OrderApiTest Class Doc Comment
 *
 * @category Class
 * @package  KlarnaCheckoutApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class OrderApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for createOrderMerchant
     *
     * Create a new order.
     *
     */
    public function testCreateOrderMerchant()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for readOrderMerchant
     *
     * Retrieve an order.
     *
     */
    public function testReadOrderMerchant()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for updateOrderMerchant
     *
     * Update an order.
     *
     */
    public function testUpdateOrderMerchant()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
