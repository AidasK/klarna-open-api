# # CustomerTokenCustomerTokenV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenCardInformation**](CustomerTokenCardInformation.md) |  | [optional]
**direct_debit** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenDirectDebitInformation**](CustomerTokenDirectDebitInformation.md) |  | [optional]
**payment_method_type** | **string** | Selected payment method |
**status** | **string** | Status of token, can be active, cancelled, suspended |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
