# KlarnaCustomerTokenApi\TokensApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrder()**](TokensApi.md#createOrder) | **POST** /customer-token/v1/tokens/{customerToken}/order | Create a new order using the customer token
[**patchCustomerToken()**](TokensApi.md#patchCustomerToken) | **PATCH** /customer-token/v1/tokens/{customerToken}/status | Update the status of a customer token
[**readCustomerToken()**](TokensApi.md#readCustomerToken) | **GET** /customer-token/v1/tokens/{customerToken} | Read customer tokens details


## `createOrder()`

```php
createOrder($customer_token, $klarna_idempotency_key, $body): \KlarnaCustomerTokenApi\Model\CustomerTokenOrder
```

Create a new order using the customer token

Use this API call to create an order using a Klarna Customer Token. Make sure you use the correct token ID when placing an order, to ensure that the right consumer gets billed.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCustomerTokenApi\Api\TokensApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_token = 'customer_token_example'; // string
$klarna_idempotency_key = 'klarna_idempotency_key_example'; // string
$body = new \KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenOrder(); // \KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenOrder

try {
    $result = $apiInstance->createOrder($customer_token, $klarna_idempotency_key, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokensApi->createOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_token** | **string**|  |
 **klarna_idempotency_key** | **string**|  | [optional]
 **body** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenOrder**](../Model/CustomerTokenCustomerTokenOrder.md)|  | [optional]

### Return type

[**\KlarnaCustomerTokenApi\Model\CustomerTokenOrder**](../Model/CustomerTokenOrder.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `patchCustomerToken()`

```php
patchCustomerToken($customer_token, $body)
```

Update the status of a customer token

Use this API call to update the status of a Klarna Customer Token. This should be used if you want to cancel a specific customer token.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCustomerTokenApi\Api\TokensApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_token = 'customer_token_example'; // string
$body = new \KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenStatusUpdateRequest(); // \KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenStatusUpdateRequest

try {
    $apiInstance->patchCustomerToken($customer_token, $body);
} catch (Exception $e) {
    echo 'Exception when calling TokensApi->patchCustomerToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_token** | **string**|  |
 **body** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenStatusUpdateRequest**](../Model/CustomerTokenCustomerTokenStatusUpdateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readCustomerToken()`

```php
readCustomerToken($customer_token): \KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenV1
```

Read customer tokens details

Use this API call to read the content of a Klarna Customer Token. The read will return the status of the token as well as payment method details.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCustomerTokenApi\Api\TokensApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_token = 'customer_token_example'; // string

try {
    $result = $apiInstance->readCustomerToken($customer_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokensApi->readCustomerToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_token** | **string**|  |

### Return type

[**\KlarnaCustomerTokenApi\Model\CustomerTokenCustomerTokenV1**](../Model/CustomerTokenCustomerTokenV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
