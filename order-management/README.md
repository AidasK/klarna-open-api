# OpenAPIClient-php

The Order Management API is used for handling an order after the customer has completed the purchase. It is used for all actions you need to manage your orders. Examples being: updating, capturing, reading and refunding an order.

You can read more about the order management process [here](/order-management/).


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/ekomlita/klarna-open-api.git"
    }
  ],
  "require": {
    "ekomlita/klarna-open-api": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new KlarnaOrderManagementApi\Api\CapturesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = 'order_id_example'; // string | Order id
$capture_id = 'capture_id_example'; // string | Capture id
$klarna_idempotency_key = 'klarna_idempotency_key_example'; // string | This header will guarantee the idempotency of the operation. The key should be unique and is recommended to be a UUID version 4. Retries of requests are safe to be applied in case of errors such as network errors, socket errors and timeouts.
$order_management_update_shipping_info = new \KlarnaOrderManagementApi\Model\OrderManagementUpdateShippingInfo(); // \KlarnaOrderManagementApi\Model\OrderManagementUpdateShippingInfo

try {
    $apiInstance->appendShippingInfo($order_id, $capture_id, $klarna_idempotency_key, $order_management_update_shipping_info);
} catch (Exception $e) {
    echo 'Exception when calling CapturesApi->appendShippingInfo: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.klarna.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CapturesApi* | [**appendShippingInfo**](docs/Api/CapturesApi.md#appendshippinginfo) | **POST** /ordermanagement/v1/orders/{order_id}/captures/{capture_id}/shipping-info | Add shipping info to a capture
*CapturesApi* | [**captureOrder**](docs/Api/CapturesApi.md#captureorder) | **POST** /ordermanagement/v1/orders/{order_id}/captures | Create capture
*CapturesApi* | [**extendDueDate**](docs/Api/CapturesApi.md#extendduedate) | **PATCH** /ordermanagement/v1/orders/{order_id}/captures/{capture_id}/extend-due-date | Extend the customer&#39;s payment due date
*CapturesApi* | [**getCapture**](docs/Api/CapturesApi.md#getcapture) | **GET** /ordermanagement/v1/orders/{order_id}/captures/{capture_id} | Get capture
*CapturesApi* | [**getCaptures**](docs/Api/CapturesApi.md#getcaptures) | **GET** /ordermanagement/v1/orders/{order_id}/captures | Get all captures for one order
*CapturesApi* | [**getOptionsForExtendDueDate**](docs/Api/CapturesApi.md#getoptionsforextendduedate) | **GET** /ordermanagement/v1/orders/{order_id}/captures/{capture_id}/extend-due-date-options | Get available options for extension of the customer&#39;s payment due date
*CapturesApi* | [**triggerSendOut**](docs/Api/CapturesApi.md#triggersendout) | **POST** /ordermanagement/v1/orders/{order_id}/captures/{capture_id}/trigger-send-out | Trigger resend of customer communication
*OrdersApi* | [**acknowledgeOrder**](docs/Api/OrdersApi.md#acknowledgeorder) | **POST** /ordermanagement/v1/orders/{order_id}/acknowledge | Acknowledge order
*OrdersApi* | [**cancelOrder**](docs/Api/OrdersApi.md#cancelorder) | **POST** /ordermanagement/v1/orders/{order_id}/cancel | Cancel order
*OrdersApi* | [**extendAuthorizationTime**](docs/Api/OrdersApi.md#extendauthorizationtime) | **POST** /ordermanagement/v1/orders/{order_id}/extend-authorization-time | Extend authorization time
*OrdersApi* | [**getOrder**](docs/Api/OrdersApi.md#getorder) | **GET** /ordermanagement/v1/orders/{order_id} | Get order
*OrdersApi* | [**releaseRemainingAuthorization**](docs/Api/OrdersApi.md#releaseremainingauthorization) | **POST** /ordermanagement/v1/orders/{order_id}/release-remaining-authorization | Release remaining authorization
*OrdersApi* | [**updateAuthorization**](docs/Api/OrdersApi.md#updateauthorization) | **PATCH** /ordermanagement/v1/orders/{order_id}/authorization | Set new order amount and order lines
*OrdersApi* | [**updateConsumerDetails**](docs/Api/OrdersApi.md#updateconsumerdetails) | **PATCH** /ordermanagement/v1/orders/{order_id}/customer-details | Update customer addresses
*OrdersApi* | [**updateMerchantReferences**](docs/Api/OrdersApi.md#updatemerchantreferences) | **PATCH** /ordermanagement/v1/orders/{order_id}/merchant-references | Update merchant references
*RefundsApi* | [**get**](docs/Api/RefundsApi.md#get) | **GET** /ordermanagement/v1/orders/{order_id}/refunds/{refund_id} | Get refund
*RefundsApi* | [**refundOrder**](docs/Api/RefundsApi.md#refundorder) | **POST** /ordermanagement/v1/orders/{order_id}/refunds | Create a refund

## Models

- [OrderManagementAddon](docs/Model/OrderManagementAddon.md)
- [OrderManagementAddress](docs/Model/OrderManagementAddress.md)
- [OrderManagementCancelNotAllowedErrorMessage](docs/Model/OrderManagementCancelNotAllowedErrorMessage.md)
- [OrderManagementCapture](docs/Model/OrderManagementCapture.md)
- [OrderManagementCaptureNotAllowedErrorMessage](docs/Model/OrderManagementCaptureNotAllowedErrorMessage.md)
- [OrderManagementCaptureObject](docs/Model/OrderManagementCaptureObject.md)
- [OrderManagementCarrierProduct](docs/Model/OrderManagementCarrierProduct.md)
- [OrderManagementCustomer](docs/Model/OrderManagementCustomer.md)
- [OrderManagementErrorMessageDto](docs/Model/OrderManagementErrorMessageDto.md)
- [OrderManagementExtendDueDateOptions](docs/Model/OrderManagementExtendDueDateOptions.md)
- [OrderManagementExtendDueDateRequest](docs/Model/OrderManagementExtendDueDateRequest.md)
- [OrderManagementInitialPaymentMethodDto](docs/Model/OrderManagementInitialPaymentMethodDto.md)
- [OrderManagementLocation](docs/Model/OrderManagementLocation.md)
- [OrderManagementNoSuchCaptureErrorMessage](docs/Model/OrderManagementNoSuchCaptureErrorMessage.md)
- [OrderManagementNoSuchOrderErrorMessage](docs/Model/OrderManagementNoSuchOrderErrorMessage.md)
- [OrderManagementNotAllowedErrorMessage](docs/Model/OrderManagementNotAllowedErrorMessage.md)
- [OrderManagementNotFoundErrorMessage](docs/Model/OrderManagementNotFoundErrorMessage.md)
- [OrderManagementOptionDto](docs/Model/OrderManagementOptionDto.md)
- [OrderManagementOrder](docs/Model/OrderManagementOrder.md)
- [OrderManagementOrderLine](docs/Model/OrderManagementOrderLine.md)
- [OrderManagementProductIdentifiers](docs/Model/OrderManagementProductIdentifiers.md)
- [OrderManagementRefund](docs/Model/OrderManagementRefund.md)
- [OrderManagementRefundNotAllowedErrorMessage](docs/Model/OrderManagementRefundNotAllowedErrorMessage.md)
- [OrderManagementRefundObject](docs/Model/OrderManagementRefundObject.md)
- [OrderManagementSelectedShippingOptionDto](docs/Model/OrderManagementSelectedShippingOptionDto.md)
- [OrderManagementShippingInfo](docs/Model/OrderManagementShippingInfo.md)
- [OrderManagementTimeslot](docs/Model/OrderManagementTimeslot.md)
- [OrderManagementUpdateAuthorization](docs/Model/OrderManagementUpdateAuthorization.md)
- [OrderManagementUpdateConsumer](docs/Model/OrderManagementUpdateConsumer.md)
- [OrderManagementUpdateMerchantReferences](docs/Model/OrderManagementUpdateMerchantReferences.md)
- [OrderManagementUpdateShippingInfo](docs/Model/OrderManagementUpdateShippingInfo.md)

## Authorization

### basicAuth

- **Type**: HTTP basic authentication

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
