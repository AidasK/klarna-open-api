# # XsATransactionsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **string** | ID of the user&#39;s account | [optional]
**transactions** | [**\KlarnaXsAApi\Model\XsATransaction[]**](XsATransaction.md) | Array of transactions | [optional]
**page_info** | [**\KlarnaXsAApi\Model\XsAPageInfo**](XsAPageInfo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
