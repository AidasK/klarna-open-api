# # XsAPostConsentSessionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the created consent session. | [optional]
**status** | **string** | Status of the created consent session. | [optional]
**consent_id** | **string** | ID of the created consent once the consent session was approved by the PSU. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
