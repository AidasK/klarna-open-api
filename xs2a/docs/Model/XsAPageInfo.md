# # XsAPageInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_number** | **int** | Page number | [optional]
**page_size** | **int** | Page size | [optional]
**total_elements** | **int** | Number of total transactions | [optional]
**total_pages** | **int** | Number of total pages given the page size and total elements | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
