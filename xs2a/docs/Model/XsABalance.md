# # XsABalance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | Balance amount in the smallest unit for the given currency. For EUR, it returns the balance in cents. | [optional]
**currency** | **string** | Account currency. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
