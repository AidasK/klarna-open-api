# # MerchantCardServicePromiseResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promise_id** | **string** | The unique promise ID | [optional] [readonly]
**order_id** | **string** | The order id of the promise | [optional] [readonly]
**cards** | [**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCardSpecification[]**](MerchantCardServiceCardSpecification.md) | The list of card specifications | [optional] [readonly]
**created_at** | **\DateTime** | The time when the promise was created | [optional] [readonly]
**expire_at** | **\DateTime** | The time when the promise expires | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
