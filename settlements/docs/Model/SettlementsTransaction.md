# # SettlementsTransaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **int** | Total amount of the specific transaction, in minor units |
**capture_id** | **string** | The Klarna assigned id reference of a specific capture |
**merchant_reference1** | **string** | Merchant assigned reference, typically a reference to an order management system id | [optional]
**sale_date** | **\DateTime** | ISO 8601 formatted date-time string |
**type** | **string** | The type of transaction. |
**capture_date** | **\DateTime** | ISO 8601 formatted date-time string |
**payment_reference** | **string** | Reference to the specific payout the transaction is part of, if available. | [optional]
**order_id** | **string** | The Klarna assigned order id reference |
**payout** | **string** | Link to the payout that this transaction is part of | [optional]
**refund_id** | **string** | The Klarna assigned id reference of a specific refund | [optional]
**short_order_id** | **string** | The Klarna assigned short order id reference | [optional]
**merchant_reference2** | **string** | Merchant assigned reference, typically a reference to an order management system id | [optional]
**currency_code** | **string** | ISO 4217 Currency Code. Like USD, EUR, AUD or GBP. |
**purchase_country** | **string** | ISO Alpha-2 Country Code |
**vat_rate** | **int** | VAT (Value added tax) rate on Klarna fees | [optional]
**vat_amount** | **int** | VAT (Value added tax) amount on Klarna fees, in minor units | [optional]
**shipping_country** | **string** | ISO Alpha-2 Country Code | [optional]
**initial_payment_method_type** | **string** | Payment method the consumer chose during checkout | [optional]
**initial_number_of_installments** | **int** | Number of installments the consumer chose during checkout in case of installment payments | [optional]
**initial_payment_method_monthly_downpayments** | **int** | Number of monthly downpayments that were chosen during the checkout in case of installment payments. | [optional]
**merchant_capture_reference** | **string** | Your internal reference to the capture, that has been submitted during capturing an order via API | [optional]
**merchant_refund_reference** | **string** | Your internal reference to the refund, that has been submitted during refunding an order via API | [optional]
**detailed_type** | **string** | Detailed description of the transaction type | [optional]
**tax_in_currency_of_registration_country** | **int** | The tax amount on the respective fee, converted into the currency of your registration country. In case you are a German merchant selling in another currency then EUR or a Swedish merchant selling in another currency then SEK, we convert the VAT amount on the Klarna fees into the currency of the country you are registered in, based on the exchange rate of the capture date. | [optional]
**currency_code_of_registration_country** | **string** | ISO 4217 Currency Code of the country you are registered in. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
