# # SettlementsTransactionCollection

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**\KlarnaSettlementsApi\Model\SettlementsTransaction[]**](SettlementsTransaction.md) |  |
**pagination** | [**\KlarnaSettlementsApi\Model\SettlementsPagination**](SettlementsPagination.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
