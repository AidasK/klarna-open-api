# KlarnaSettlementsApi\ReportsApi

All URIs are relative to https://api.klarna.com/settlements/v1.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPayoutReportWithTransactions()**](ReportsApi.md#getPayoutReportWithTransactions) | **GET** /reports/payout-with-transactions | Get CSV payout report
[**getPayoutsSummaryReportWithTransactions()**](ReportsApi.md#getPayoutsSummaryReportWithTransactions) | **GET** /reports/payouts-summary-with-transactions | Get CSV summary
[**payout()**](ReportsApi.md#payout) | **GET** /reports/payout | Get PDF payout summary report
[**payoutsSummary()**](ReportsApi.md#payoutsSummary) | **GET** /reports/payouts-summary | Get PDF summary


## `getPayoutReportWithTransactions()`

```php
getPayoutReportWithTransactions($payment_reference)
```

Get CSV payout report

More information about this CSV format is available at:                                      https://developers.klarna.com/en/gb/kco-v3/settlement-files

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\ReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_reference = 'payment_reference_example'; // string | The reference id of the payout

try {
    $apiInstance->getPayoutReportWithTransactions($payment_reference);
} catch (Exception $e) {
    echo 'Exception when calling ReportsApi->getPayoutReportWithTransactions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_reference** | **string**| The reference id of the payout |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `text/csv`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPayoutsSummaryReportWithTransactions()`

```php
getPayoutsSummaryReportWithTransactions($start_date, $end_date)
```

Get CSV summary

More information about this CSV format is available at:                                      https://developers.klarna.com/en/gb/kco-v3/settlement-files

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\ReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -> 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z.
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z.

try {
    $apiInstance->getPayoutsSummaryReportWithTransactions($start_date, $end_date);
} catch (Exception $e) {
    echo 'Exception when calling ReportsApi->getPayoutsSummaryReportWithTransactions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -&gt; 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z. |
 **end_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `text/csv`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `payout()`

```php
payout($payment_reference)
```

Get PDF payout summary report

A single settlement summed up in pdf format

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\ReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_reference = 'payment_reference_example'; // string | The reference id of the payout

try {
    $apiInstance->payout($payment_reference);
} catch (Exception $e) {
    echo 'Exception when calling ReportsApi->payout: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_reference** | **string**| The reference id of the payout |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `payoutsSummary()`

```php
payoutsSummary($start_date, $end_date)
```

Get PDF summary

Returns a summary for all payouts between the given dates

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\ReportsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -> 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z.
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z.

try {
    $apiInstance->payoutsSummary($start_date, $end_date);
} catch (Exception $e) {
    echo 'Exception when calling ReportsApi->payoutsSummary: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -&gt; 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z. |
 **end_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z. |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
