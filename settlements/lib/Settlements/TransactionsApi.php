<?php
/**
 * TransactionsApi
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Settlements API
 *
 * The Settlements API helps you with the reconciliation of payments, made by Klarna to your bank account. Every payment has a unique payment_reference that can be found in the settlement reports and on your bank statement.  Besides responses in JSON-format, we provide endpoints to download transactional CSV reports, as well as PDF Summary, reports. Both reports can be downloaded for a specific payout as well as aggregated for a time range.  ![](https://images.prismic.io/docsportal/b20b8140-ea4e-4b0a-a812-dfb459aa332d_FIRE-settlements_api_overview.png?auto=compress,format)  For the day-to-day reconciliation of payments we recommend the following:  - By performing the requests “get all payouts” you will get the totals of the payouts and the corresponding payment references as a response. The payment references are needed for the request “get transactions”.  - Perform the request \"get transactions\". The response contains all related transactions of a payout.  - If you prefer to use CSV-files for your reconciliation, you can query “Get CSV payout report” for a specific payment_reference.  Every transaction is related to a unique order_ID. All related transactions in the life-span of an order are associated with this ID. eg. fees or refunds. It is, therefore, the recommended identifier for reconciling the report lines with your system.  Resources are split into two broad types:  - Collections, including pagination information: collections are queryable, typically by the attributes of the sub-resource as well as pagination.  - Entity resources containing a single entity.
 *
 * The version of the OpenAPI document: 1.0.0-rc2
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaSettlementsApi\Settlements;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use KlarnaSettlementsApi\ApiException;
use KlarnaSettlementsApi\Configuration;
use KlarnaSettlementsApi\HeaderSelector;
use KlarnaSettlementsApi\ObjectSerializer;

/**
 * TransactionsApi Class Doc Comment
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class TransactionsApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @var int Host index
     */
    protected $hostIndex;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     * @param int             $hostIndex (Optional) host index to select the list of hosts if defined in the OpenAPI spec
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null,
        $hostIndex = 0
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
        $this->hostIndex = $hostIndex;
    }

    /**
     * Set the host index
     *
     * @param int $hostIndex Host index (required)
     */
    public function setHostIndex($hostIndex): void
    {
        $this->hostIndex = $hostIndex;
    }

    /**
     * Get the host index
     *
     * @return int Host index
     */
    public function getHostIndex()
    {
        return $this->hostIndex;
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation getTransactions
     *
     * Get transactions
     *
     * @param  string $payment_reference The reference id of the payout (optional)
     * @param  string $order_id The Klarna assigned order id reference (optional)
     * @param  int $size How many elements to include in the result. If no value for size is provided, a default of 20 will be used. (optional, default to 20)
     * @param  int $offset The current offset. Describes \&quot;where\&quot; in a collection the current starts. (optional, default to 0)
     *
     * @throws \KlarnaSettlementsApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \KlarnaSettlementsApi\Model\SettlementsTransactionCollection|\KlarnaSettlementsApi\Model\SettlementsErrorResponse
     */
    public function getTransactions($payment_reference = null, $order_id = null, $size = 20, $offset = 0)
    {
        list($response) = $this->getTransactionsWithHttpInfo($payment_reference, $order_id, $size, $offset);
        return $response;
    }

    /**
     * Operation getTransactionsWithHttpInfo
     *
     * Get transactions
     *
     * @param  string $payment_reference The reference id of the payout (optional)
     * @param  string $order_id The Klarna assigned order id reference (optional)
     * @param  int $size How many elements to include in the result. If no value for size is provided, a default of 20 will be used. (optional, default to 20)
     * @param  int $offset The current offset. Describes \&quot;where\&quot; in a collection the current starts. (optional, default to 0)
     *
     * @throws \KlarnaSettlementsApi\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \KlarnaSettlementsApi\Model\SettlementsTransactionCollection|\KlarnaSettlementsApi\Model\SettlementsErrorResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function getTransactionsWithHttpInfo($payment_reference = null, $order_id = null, $size = 20, $offset = 0)
    {
        $request = $this->getTransactionsRequest($payment_reference, $order_id, $size, $offset);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    (int) $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? (string) $e->getResponse()->getBody() : null
                );
            } catch (ConnectException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    (int) $e->getCode(),
                    null,
                    null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        (string) $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    (string) $response->getBody()
                );
            }

            switch($statusCode) {
                case 200:
                    if ('\KlarnaSettlementsApi\Model\SettlementsTransactionCollection' === '\SplFileObject') {
                        $content = $response->getBody(); //stream goes to serializer
                    } else {
                        $content = (string) $response->getBody();
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\KlarnaSettlementsApi\Model\SettlementsTransactionCollection', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                case 400:
                    if ('\KlarnaSettlementsApi\Model\SettlementsErrorResponse' === '\SplFileObject') {
                        $content = $response->getBody(); //stream goes to serializer
                    } else {
                        $content = (string) $response->getBody();
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\KlarnaSettlementsApi\Model\SettlementsErrorResponse', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
            }

            $returnType = '\KlarnaSettlementsApi\Model\SettlementsTransactionCollection';
            if ($returnType === '\SplFileObject') {
                $content = $response->getBody(); //stream goes to serializer
            } else {
                $content = (string) $response->getBody();
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\KlarnaSettlementsApi\Model\SettlementsTransactionCollection',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\KlarnaSettlementsApi\Model\SettlementsErrorResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation getTransactionsAsync
     *
     * Get transactions
     *
     * @param  string $payment_reference The reference id of the payout (optional)
     * @param  string $order_id The Klarna assigned order id reference (optional)
     * @param  int $size How many elements to include in the result. If no value for size is provided, a default of 20 will be used. (optional, default to 20)
     * @param  int $offset The current offset. Describes \&quot;where\&quot; in a collection the current starts. (optional, default to 0)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getTransactionsAsync($payment_reference = null, $order_id = null, $size = 20, $offset = 0)
    {
        return $this->getTransactionsAsyncWithHttpInfo($payment_reference, $order_id, $size, $offset)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation getTransactionsAsyncWithHttpInfo
     *
     * Get transactions
     *
     * @param  string $payment_reference The reference id of the payout (optional)
     * @param  string $order_id The Klarna assigned order id reference (optional)
     * @param  int $size How many elements to include in the result. If no value for size is provided, a default of 20 will be used. (optional, default to 20)
     * @param  int $offset The current offset. Describes \&quot;where\&quot; in a collection the current starts. (optional, default to 0)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getTransactionsAsyncWithHttpInfo($payment_reference = null, $order_id = null, $size = 20, $offset = 0)
    {
        $returnType = '\KlarnaSettlementsApi\Model\SettlementsTransactionCollection';
        $request = $this->getTransactionsRequest($payment_reference, $order_id, $size, $offset);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    if ($returnType === '\SplFileObject') {
                        $content = $response->getBody(); //stream goes to serializer
                    } else {
                        $content = (string) $response->getBody();
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        (string) $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'getTransactions'
     *
     * @param  string $payment_reference The reference id of the payout (optional)
     * @param  string $order_id The Klarna assigned order id reference (optional)
     * @param  int $size How many elements to include in the result. If no value for size is provided, a default of 20 will be used. (optional, default to 20)
     * @param  int $offset The current offset. Describes \&quot;where\&quot; in a collection the current starts. (optional, default to 0)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    public function getTransactionsRequest($payment_reference = null, $order_id = null, $size = 20, $offset = 0)
    {

        $resourcePath = '/transactions';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;

        // query params
        if ($payment_reference !== null) {
            if('form' === 'form' && is_array($payment_reference)) {
                foreach($payment_reference as $key => $value) {
                    $queryParams[$key] = $value;
                }
            }
            else {
                $queryParams['payment_reference'] = $payment_reference;
            }
        }
        // query params
        if ($order_id !== null) {
            if('form' === 'form' && is_array($order_id)) {
                foreach($order_id as $key => $value) {
                    $queryParams[$key] = $value;
                }
            }
            else {
                $queryParams['order_id'] = $order_id;
            }
        }
        // query params
        if ($size !== null) {
            if('form' === 'form' && is_array($size)) {
                foreach($size as $key => $value) {
                    $queryParams[$key] = $value;
                }
            }
            else {
                $queryParams['size'] = $size;
            }
        }
        // query params
        if ($offset !== null) {
            if('form' === 'form' && is_array($offset)) {
                foreach($offset as $key => $value) {
                    $queryParams[$key] = $value;
                }
            }
            else {
                $queryParams['offset'] = $offset;
            }
        }




        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                []
            );
        }

        // for model (json/xml)
        if (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $formParamValueItems = is_array($formParamValue) ? $formParamValue : [$formParamValue];
                    foreach ($formParamValueItems as $formParamValueItem) {
                        $multipartContents[] = [
                            'name' => $formParamName,
                            'contents' => $formParamValueItem
                        ];
                    }
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'GET',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
