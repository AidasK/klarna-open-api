# OpenAPIClient-php

The Settlements API helps you with the reconciliation of payments, made by Klarna to your bank account. Every payment has a unique payment_reference that can be found in the settlement reports and on your bank statement.

Besides responses in JSON-format, we provide endpoints to download transactional CSV reports, as well as PDF Summary, reports. Both reports can be downloaded for a specific payout as well as aggregated for a time range.

![](https://images.prismic.io/docsportal/b20b8140-ea4e-4b0a-a812-dfb459aa332d_FIRE-settlements_api_overview.png?auto=compress,format)

For the day-to-day reconciliation of payments we recommend the following:

- By performing the requests “get all payouts” you will get the totals of the payouts and the corresponding payment references as a response. The payment references are needed for the request “get transactions”.

- Perform the request \"get transactions\". The response contains all related transactions of a payout.

- If you prefer to use CSV-files for your reconciliation, you can query “Get CSV payout report” for a specific payment_reference.

Every transaction is related to a unique order_ID. All related transactions in the life-span of an order are associated with this ID. eg. fees or refunds. It is, therefore, the recommended identifier for reconciling the report lines with your system.

Resources are split into two broad types:

- Collections, including pagination information: collections are queryable, typically by the attributes of the sub-resource as well as pagination.

- Entity resources containing a single entity.


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/ekomlita/klarna-open-api.git"
    }
  ],
  "require": {
    "ekomlita/klarna-open-api": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new KlarnaSettlementsApi\Api\PayoutsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_reference = 'payment_reference_example'; // string | The reference id of the payout. Normally this reference can be found on your payment slip statement of your bank.

try {
    $result = $apiInstance->getPayout($payment_reference);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayoutsApi->getPayout: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.klarna.com/settlements/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*PayoutsApi* | [**getPayout**](docs/Api/PayoutsApi.md#getpayout) | **GET** /payouts/{payment_reference} | Get payout
*PayoutsApi* | [**getPayoutSummary**](docs/Api/PayoutsApi.md#getpayoutsummary) | **GET** /payouts/summary | Get summary of payouts
*PayoutsApi* | [**getPayouts**](docs/Api/PayoutsApi.md#getpayouts) | **GET** /payouts | Get all payouts
*ReportsApi* | [**getPayoutReportWithTransactions**](docs/Api/ReportsApi.md#getpayoutreportwithtransactions) | **GET** /reports/payout-with-transactions | Get CSV payout report
*ReportsApi* | [**getPayoutsSummaryReportWithTransactions**](docs/Api/ReportsApi.md#getpayoutssummaryreportwithtransactions) | **GET** /reports/payouts-summary-with-transactions | Get CSV summary
*ReportsApi* | [**payout**](docs/Api/ReportsApi.md#payout) | **GET** /reports/payout | Get PDF payout summary report
*ReportsApi* | [**payoutsSummary**](docs/Api/ReportsApi.md#payoutssummary) | **GET** /reports/payouts-summary | Get PDF summary
*TransactionsApi* | [**getTransactions**](docs/Api/TransactionsApi.md#gettransactions) | **GET** /transactions | Get transactions

## Models

- [SettlementsErrorResponse](docs/Model/SettlementsErrorResponse.md)
- [SettlementsPagination](docs/Model/SettlementsPagination.md)
- [SettlementsPayout](docs/Model/SettlementsPayout.md)
- [SettlementsPayoutCollection](docs/Model/SettlementsPayoutCollection.md)
- [SettlementsPayoutSummary](docs/Model/SettlementsPayoutSummary.md)
- [SettlementsTotals](docs/Model/SettlementsTotals.md)
- [SettlementsTransaction](docs/Model/SettlementsTransaction.md)
- [SettlementsTransactionCollection](docs/Model/SettlementsTransactionCollection.md)

## Authorization

### basic_auth

- **Type**: HTTP basic authentication

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0-rc2`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
