<?php
/**
 * SettlementsPayoutCollectionTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Settlements API
 *
 * This API gives you access to your payouts and transactions.     Resources are split into two broad types:     * Collections, including pagination information:      collections are queryable, typically by the attributes of the sub-resource      as well as pagination.    * Entity resources containing a single entity.
 *
 * The version of the OpenAPI document: 1.0.0-rc2
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaSettlementsApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * SettlementsPayoutCollectionTest Class Doc Comment
 *
 * @category    Class
 * @description SettlementsPayoutCollection
 * @package     KlarnaSettlementsApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SettlementsPayoutCollectionTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "SettlementsPayoutCollection"
     */
    public function testSettlementsPayoutCollection()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payouts"
     */
    public function testPropertyPayouts()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "pagination"
     */
    public function testPropertyPagination()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
