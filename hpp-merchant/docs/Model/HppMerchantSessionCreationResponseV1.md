# # HppMerchantSessionCreationResponseV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distribution_url** | **string** | Endpoint for link distribution | [optional]
**qr_code_url** | **string** | HPP url to download qr code image | [optional]
**redirect_url** | **string** | HPP url to redirect the consumer to. ECOMMERCE only | [optional]
**session_id** | **string** | HPP session id | [optional]
**session_url** | **string** | Endpoint to read the session | [optional]
**manual_identification_check_url** | **string** | Endpoint for manual identification check | [optional]
**expires_at** | **string** | Session expiration time | [optional]
**distribution_module** | [**\KlarnaHppMerchantApi\Model\HppMerchantDistributionModuleV1**](HppMerchantDistributionModuleV1.md) |  | [optional]
**qr_code_embedded_url** | **string** | HPP url to generate qr code. IN_STORE only | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
