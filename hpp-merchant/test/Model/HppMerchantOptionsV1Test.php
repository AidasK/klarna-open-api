<?php
/**
 * HppMerchantOptionsV1Test
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaHppMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * HPP
 *
 * Hosted Payment Page
 *
 * The version of the OpenAPI document: 1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaHppMerchantApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * HppMerchantOptionsV1Test Class Doc Comment
 *
 * @category    Class
 * @description HppMerchantOptionsV1
 * @package     KlarnaHppMerchantApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class HppMerchantOptionsV1Test extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "HppMerchantOptionsV1"
     */
    public function testHppMerchantOptionsV1()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "background_images"
     */
    public function testPropertyBackgroundImages()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "logo_url"
     */
    public function testPropertyLogoUrl()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "page_title"
     */
    public function testPropertyPageTitle()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_fallback"
     */
    public function testPropertyPaymentFallback()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_method_categories"
     */
    public function testPropertyPaymentMethodCategories()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_method_category"
     */
    public function testPropertyPaymentMethodCategory()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "purchase_type"
     */
    public function testPropertyPurchaseType()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
