<?php
/**
 * SessionsApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaHppMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * HPP
 *
 * Hosted Payment Page
 *
 * The version of the OpenAPI document: 1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace KlarnaHppMerchantApi\Test\Api;

use \KlarnaHppMerchantApi\Configuration;
use \KlarnaHppMerchantApi\ApiException;
use \KlarnaHppMerchantApi\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SessionsApiTest Class Doc Comment
 *
 * @category Class
 * @package  KlarnaHppMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SessionsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for createHppSession
     *
     * Create a new HPP session.
     *
     */
    public function testCreateHppSession()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for disableHppSession
     *
     * Disable HPP session.
     *
     */
    public function testDisableHppSession()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for distributeHppSession
     *
     * Distribute link to the HPP Session to the Consumer.
     *
     */
    public function testDistributeHppSession()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getSessionById
     *
     * Get HPP session.
     *
     */
    public function testGetSessionById()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
