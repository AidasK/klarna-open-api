# # PaymentsCustomerTokenCreationRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_address** | [**\KlarnaPaymentsApi\Model\PaymentsAddress**](PaymentsAddress.md) |  | [optional]
**customer** | [**\KlarnaPaymentsApi\Model\PaymentsCustomer**](PaymentsCustomer.md) |  | [optional]
**description** | **string** | Description of the purpose of the token. |
**intended_use** | **string** | Intended use for the token. |
**locale** | **string** | RFC 1766 customer&#39;s locale. |
**purchase_country** | **string** | ISO 3166 alpha-2 purchase country. |
**purchase_currency** | **string** | ISO 4217 purchase currency. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
