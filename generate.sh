#!/usr/bin/env bash

FILES="./specs/*"
echo "# Klarna api" > README.md
echo "" >> README.md

PSR4=""

for file in $FILES
do
  NAME=$(basename "$file" | sed -e 's/.json//')
  CAMELCASE_NAME=$(echo "$NAME" | perl -pe 's/(?:^|[^a-z])([a-z0-9])/\u$1/g')

  echo "[$CAMELCASE_NAME Api Docs]($NAME/README.md)" >> README.md
  echo "" >> README.md

  openapi-generator generate -i "$file" \
    -g php \
    -o "$NAME" \
    --model-name-prefix "$CAMELCASE_NAME" \
    --api-package "$CAMELCASE_NAME" \
    --git-user-id aidask \
    --git-repo-id klarna-open-api \
    --invoker-package "Klarna${CAMELCASE_NAME}Api"
  PSR4="$PSR4,
            \"Klarna${CAMELCASE_NAME}Api\\\\\" : \"$NAME/lib/\""
done

echo "" >> README.md
echo '## You can regenerate apis by renewing spec files and launching `./generate.sh`' >> README.md
echo "" >> README.md

export PSR4=$(echo "$PSR4" | sed 's/^,//')
envsubst < composer-template.json > composer.json