<?php
/**
 * CheckoutMerchantShippingServiceRequest
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaCheckoutMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Checkout API V3
 *
 * Klarna provides a number of different callbacks during the customer's checkout session. The callbacks can be used to update the order when the customer changes their address as well as to do a final check to validate the order before it is submitted.  The callbacks are sent as POST requests to the endpoint you specify when creating the order. Your response is interpreted using the response code and body.
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaCheckoutMerchantApi\Model;

use \ArrayAccess;
use \KlarnaCheckoutMerchantApi\ObjectSerializer;

/**
 * CheckoutMerchantShippingServiceRequest Class Doc Comment
 *
 * @category Class
 * @package  KlarnaCheckoutMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class CheckoutMerchantShippingServiceRequest implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'shipping_service_request';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'customer' => '\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantCustomer',
        'locale' => 'string',
        'order_amount' => 'int',
        'order_tax_amount' => 'int',
        'order_lines' => '\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrderLine[]',
        'billing_address' => '\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1',
        'shipping_address' => '\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1',
        'selected_shipping_option' => '\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingOption',
        'purchase_currency' => 'string',
        'merchant_data' => 'string',
        'merchant_reference1' => 'string',
        'merchant_reference2' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'customer' => null,
        'locale' => null,
        'order_amount' => 'int64',
        'order_tax_amount' => 'int64',
        'order_lines' => null,
        'billing_address' => null,
        'shipping_address' => null,
        'selected_shipping_option' => null,
        'purchase_currency' => null,
        'merchant_data' => null,
        'merchant_reference1' => null,
        'merchant_reference2' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'customer' => 'customer',
        'locale' => 'locale',
        'order_amount' => 'order_amount',
        'order_tax_amount' => 'order_tax_amount',
        'order_lines' => 'order_lines',
        'billing_address' => 'billing_address',
        'shipping_address' => 'shipping_address',
        'selected_shipping_option' => 'selected_shipping_option',
        'purchase_currency' => 'purchase_currency',
        'merchant_data' => 'merchant_data',
        'merchant_reference1' => 'merchant_reference1',
        'merchant_reference2' => 'merchant_reference2'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'customer' => 'setCustomer',
        'locale' => 'setLocale',
        'order_amount' => 'setOrderAmount',
        'order_tax_amount' => 'setOrderTaxAmount',
        'order_lines' => 'setOrderLines',
        'billing_address' => 'setBillingAddress',
        'shipping_address' => 'setShippingAddress',
        'selected_shipping_option' => 'setSelectedShippingOption',
        'purchase_currency' => 'setPurchaseCurrency',
        'merchant_data' => 'setMerchantData',
        'merchant_reference1' => 'setMerchantReference1',
        'merchant_reference2' => 'setMerchantReference2'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'customer' => 'getCustomer',
        'locale' => 'getLocale',
        'order_amount' => 'getOrderAmount',
        'order_tax_amount' => 'getOrderTaxAmount',
        'order_lines' => 'getOrderLines',
        'billing_address' => 'getBillingAddress',
        'shipping_address' => 'getShippingAddress',
        'selected_shipping_option' => 'getSelectedShippingOption',
        'purchase_currency' => 'getPurchaseCurrency',
        'merchant_data' => 'getMerchantData',
        'merchant_reference1' => 'getMerchantReference1',
        'merchant_reference2' => 'getMerchantReference2'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['customer'] = $data['customer'] ?? null;
        $this->container['locale'] = $data['locale'] ?? null;
        $this->container['order_amount'] = $data['order_amount'] ?? null;
        $this->container['order_tax_amount'] = $data['order_tax_amount'] ?? null;
        $this->container['order_lines'] = $data['order_lines'] ?? null;
        $this->container['billing_address'] = $data['billing_address'] ?? null;
        $this->container['shipping_address'] = $data['shipping_address'] ?? null;
        $this->container['selected_shipping_option'] = $data['selected_shipping_option'] ?? null;
        $this->container['purchase_currency'] = $data['purchase_currency'] ?? null;
        $this->container['merchant_data'] = $data['merchant_data'] ?? null;
        $this->container['merchant_reference1'] = $data['merchant_reference1'] ?? null;
        $this->container['merchant_reference2'] = $data['merchant_reference2'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['order_amount'] === null) {
            $invalidProperties[] = "'order_amount' can't be null";
        }
        if (($this->container['order_amount'] < 0)) {
            $invalidProperties[] = "invalid value for 'order_amount', must be bigger than or equal to 0.";
        }

        if ($this->container['order_tax_amount'] === null) {
            $invalidProperties[] = "'order_tax_amount' can't be null";
        }
        if (($this->container['order_tax_amount'] < 0)) {
            $invalidProperties[] = "invalid value for 'order_tax_amount', must be bigger than or equal to 0.";
        }

        if ($this->container['order_lines'] === null) {
            $invalidProperties[] = "'order_lines' can't be null";
        }
        if ((count($this->container['order_lines']) > 1000)) {
            $invalidProperties[] = "invalid value for 'order_lines', number of items must be less than or equal to 1000.";
        }

        if ((count($this->container['order_lines']) < 0)) {
            $invalidProperties[] = "invalid value for 'order_lines', number of items must be greater than or equal to 0.";
        }

        if ($this->container['purchase_currency'] === null) {
            $invalidProperties[] = "'purchase_currency' can't be null";
        }
        if (!preg_match("/^[A-Za-z]{3,3}$/", $this->container['purchase_currency'])) {
            $invalidProperties[] = "invalid value for 'purchase_currency', must be conform to the pattern /^[A-Za-z]{3,3}$/.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets customer
     *
     * @return \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantCustomer|null
     */
    public function getCustomer()
    {
        return $this->container['customer'];
    }

    /**
     * Sets customer
     *
     * @param \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantCustomer|null $customer customer
     *
     * @return self
     */
    public function setCustomer($customer)
    {
        $this->container['customer'] = $customer;

        return $this;
    }

    /**
     * Gets locale
     *
     * @return string|null
     */
    public function getLocale()
    {
        return $this->container['locale'];
    }

    /**
     * Sets locale
     *
     * @param string|null $locale RFC 1766 customer's locale.
     *
     * @return self
     */
    public function setLocale($locale)
    {
        $this->container['locale'] = $locale;

        return $this;
    }

    /**
     * Gets order_amount
     *
     * @return int
     */
    public function getOrderAmount()
    {
        return $this->container['order_amount'];
    }

    /**
     * Sets order_amount
     *
     * @param int $order_amount Non-negative, minor units. Total total amount of the order, including tax and any discounts.
     *
     * @return self
     */
    public function setOrderAmount($order_amount)
    {

        if (($order_amount < 0)) {
            throw new \InvalidArgumentException('invalid value for $order_amount when calling CheckoutMerchantShippingServiceRequest., must be bigger than or equal to 0.');
        }

        $this->container['order_amount'] = $order_amount;

        return $this;
    }

    /**
     * Gets order_tax_amount
     *
     * @return int
     */
    public function getOrderTaxAmount()
    {
        return $this->container['order_tax_amount'];
    }

    /**
     * Sets order_tax_amount
     *
     * @param int $order_tax_amount Non-negative, minor units. The total tax amount of the order.
     *
     * @return self
     */
    public function setOrderTaxAmount($order_tax_amount)
    {

        if (($order_tax_amount < 0)) {
            throw new \InvalidArgumentException('invalid value for $order_tax_amount when calling CheckoutMerchantShippingServiceRequest., must be bigger than or equal to 0.');
        }

        $this->container['order_tax_amount'] = $order_tax_amount;

        return $this;
    }

    /**
     * Gets order_lines
     *
     * @return \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrderLine[]
     */
    public function getOrderLines()
    {
        return $this->container['order_lines'];
    }

    /**
     * Sets order_lines
     *
     * @param \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrderLine[] $order_lines The applicable order lines (max 1000)
     *
     * @return self
     */
    public function setOrderLines($order_lines)
    {

        if ((count($order_lines) > 1000)) {
            throw new \InvalidArgumentException('invalid value for $order_lines when calling CheckoutMerchantShippingServiceRequest., number of items must be less than or equal to 1000.');
        }
        if ((count($order_lines) < 0)) {
            throw new \InvalidArgumentException('invalid length for $order_lines when calling CheckoutMerchantShippingServiceRequest., number of items must be greater than or equal to 0.');
        }
        $this->container['order_lines'] = $order_lines;

        return $this;
    }

    /**
     * Gets billing_address
     *
     * @return \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1|null
     */
    public function getBillingAddress()
    {
        return $this->container['billing_address'];
    }

    /**
     * Sets billing_address
     *
     * @param \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1|null $billing_address billing_address
     *
     * @return self
     */
    public function setBillingAddress($billing_address)
    {
        $this->container['billing_address'] = $billing_address;

        return $this;
    }

    /**
     * Gets shipping_address
     *
     * @return \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1|null
     */
    public function getShippingAddress()
    {
        return $this->container['shipping_address'];
    }

    /**
     * Sets shipping_address
     *
     * @param \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1|null $shipping_address shipping_address
     *
     * @return self
     */
    public function setShippingAddress($shipping_address)
    {
        $this->container['shipping_address'] = $shipping_address;

        return $this;
    }

    /**
     * Gets selected_shipping_option
     *
     * @return \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingOption|null
     */
    public function getSelectedShippingOption()
    {
        return $this->container['selected_shipping_option'];
    }

    /**
     * Sets selected_shipping_option
     *
     * @param \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingOption|null $selected_shipping_option selected_shipping_option
     *
     * @return self
     */
    public function setSelectedShippingOption($selected_shipping_option)
    {
        $this->container['selected_shipping_option'] = $selected_shipping_option;

        return $this;
    }

    /**
     * Gets purchase_currency
     *
     * @return string
     */
    public function getPurchaseCurrency()
    {
        return $this->container['purchase_currency'];
    }

    /**
     * Sets purchase_currency
     *
     * @param string $purchase_currency ISO 4217 purchase currency.
     *
     * @return self
     */
    public function setPurchaseCurrency($purchase_currency)
    {

        if ((!preg_match("/^[A-Za-z]{3,3}$/", $purchase_currency))) {
            throw new \InvalidArgumentException("invalid value for $purchase_currency when calling CheckoutMerchantShippingServiceRequest., must conform to the pattern /^[A-Za-z]{3,3}$/.");
        }

        $this->container['purchase_currency'] = $purchase_currency;

        return $this;
    }

    /**
     * Gets merchant_data
     *
     * @return string|null
     */
    public function getMerchantData()
    {
        return $this->container['merchant_data'];
    }

    /**
     * Sets merchant_data
     *
     * @param string|null $merchant_data Pass through field (max 6000 characters).
     *
     * @return self
     */
    public function setMerchantData($merchant_data)
    {
        $this->container['merchant_data'] = $merchant_data;

        return $this;
    }

    /**
     * Gets merchant_reference1
     *
     * @return string|null
     */
    public function getMerchantReference1()
    {
        return $this->container['merchant_reference1'];
    }

    /**
     * Sets merchant_reference1
     *
     * @param string|null $merchant_reference1 Used for storing merchant's internal order number or other reference. If set, will be shown on the confirmation page as \"order number\" (max 255 characters).
     *
     * @return self
     */
    public function setMerchantReference1($merchant_reference1)
    {
        $this->container['merchant_reference1'] = $merchant_reference1;

        return $this;
    }

    /**
     * Gets merchant_reference2
     *
     * @return string|null
     */
    public function getMerchantReference2()
    {
        return $this->container['merchant_reference2'];
    }

    /**
     * Sets merchant_reference2
     *
     * @param string|null $merchant_reference2 Used for storing merchant's internal order number or other reference (max 255 characters).
     *
     * @return self
     */
    public function setMerchantReference2($merchant_reference2)
    {
        $this->container['merchant_reference2'] = $merchant_reference2;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


