# # CheckoutMerchantShippingAttributes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weight** | **int** | The product&#39;s weight as used in the merchant&#39;s webshop. Non-negative. Measured in grams. | [optional]
**dimensions** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantDimensions**](CheckoutMerchantDimensions.md) |  | [optional]
**tags** | **string[]** | The product&#39;s extra features, example [\&quot;dangerous_goods\&quot;, \&quot;bulky\&quot;] | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
