# # CheckoutMerchantShippingServiceErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_type** | **string** | Error type. Possible values: &lt;em&gt;unsupported_shipping_address&lt;/em&gt;, &lt;em&gt;address_error&lt;/em&gt; or &lt;em&gt;approval_failed&lt;/em&gt; |
**error_text** | **string** | Error text. Required if error_type is &lt;em&gt;address_error&lt;/em&gt; or &lt;em&gt;approval_failed&lt;/em&gt; |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
