# KlarnaCheckoutMerchantApi\CallbacksApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**countryChange()**](CallbacksApi.md#countryChange) | **POST** /merchant_urls.country_change | Country change
[**updateAddress()**](CallbacksApi.md#updateAddress) | **POST** /merchant_urls.address_update | Address update
[**updateShippingOption()**](CallbacksApi.md#updateShippingOption) | **POST** /merchant_urls.shipping_option_update | Shipping option update
[**validate()**](CallbacksApi.md#validate) | **POST** /merchant_urls.validation | Order validation


## `countryChange()`

```php
countryChange($checkout_merchant_shipping_service_request): \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse
```

Country change

Will be called whenever the consumer changes  billing address country. The order will be updated according to the response received by Checkout.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCheckoutMerchantApi\Api\CallbacksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_merchant_shipping_service_request = new \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest(); // \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest

try {
    $result = $apiInstance->countryChange($checkout_merchant_shipping_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbacksApi->countryChange: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_merchant_shipping_service_request** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest**](../Model/CheckoutMerchantShippingServiceRequest.md)|  | [optional]

### Return type

[**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse**](../Model/CheckoutMerchantShippingServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateAddress()`

```php
updateAddress($checkout_merchant_shipping_service_request): \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse
```

Address update

Will be called whenever the consumer changes  billing or shipping address. The order will be updated according to the response received by Checkout.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCheckoutMerchantApi\Api\CallbacksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_merchant_shipping_service_request = new \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest(); // \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest

try {
    $result = $apiInstance->updateAddress($checkout_merchant_shipping_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbacksApi->updateAddress: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_merchant_shipping_service_request** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest**](../Model/CheckoutMerchantShippingServiceRequest.md)|  | [optional]

### Return type

[**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse**](../Model/CheckoutMerchantShippingServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateShippingOption()`

```php
updateShippingOption($checkout_merchant_shipping_service_request): \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse
```

Shipping option update

Will be called whenever the consumer selects  a shipping option. The order will be updated according to the response received by Checkout.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCheckoutMerchantApi\Api\CallbacksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_merchant_shipping_service_request = new \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest(); // \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest

try {
    $result = $apiInstance->updateShippingOption($checkout_merchant_shipping_service_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallbacksApi->updateShippingOption: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_merchant_shipping_service_request** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceRequest**](../Model/CheckoutMerchantShippingServiceRequest.md)|  | [optional]

### Return type

[**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingServiceResponse**](../Model/CheckoutMerchantShippingServiceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `validate()`

```php
validate($checkout_merchant_order)
```

Order validation

Will be called before completing the purchase to validate the information provided by the consumer in Klarna's Checkout iframe.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaCheckoutMerchantApi\Api\CallbacksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_merchant_order = new \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrder(); // \KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrder

try {
    $apiInstance->validate($checkout_merchant_order);
} catch (Exception $e) {
    echo 'Exception when calling CallbacksApi->validate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_merchant_order** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrder**](../Model/CheckoutMerchantOrder.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `*/*`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
