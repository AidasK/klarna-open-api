# OpenAPIClient-php

The Instant Shopping API is serving two purposes:

1. to manage the orders as they result from the Instant Shopping purchase flow

1. to generate Instant Shopping Button keys necessary for setting up the Instant Shopping flow both onsite and offsite

Note that as soon as a purchase initiated through Instant Shopping is completed within Klarna, the order should be read and handled using the [Order Management API](/order-management/api/).


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/ekomlita/klarna-open-api.git"
    }
  ],
  "require": {
    "ekomlita/klarna-open-api": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new KlarnaInstantShoppingApi\Api\ButtonKeysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$button_key = 'button_key_example'; // string

try {
    $result = $apiInstance->buttonsKeyGet($button_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ButtonKeysApi->buttonsKeyGet: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.klarna.com/instantshopping*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ButtonKeysApi* | [**buttonsKeyGet**](docs/Api/ButtonKeysApi.md#buttonskeyget) | **GET** /v1/buttons/{button_key} | See the setup options for a specific button key.
*ButtonKeysApi* | [**buttonsPost**](docs/Api/ButtonKeysApi.md#buttonspost) | **POST** /v1/buttons | Create a button key based on setup options.
*ButtonKeysApi* | [**buttonsPut**](docs/Api/ButtonKeysApi.md#buttonsput) | **PUT** /v1/buttons/{button_key} | Update the setup options for a specific button key.
*OrdersApi* | [**approveAndPlaceKbbOrder**](docs/Api/OrdersApi.md#approveandplacekbborder) | **POST** /v1/authorizations/{authorization_token}/orders | Approve the authorized order and place an order identified by the authorization token
*OrdersApi* | [**declineKbbOrder**](docs/Api/OrdersApi.md#declinekbborder) | **DELETE** /v1/authorizations/{authorization_token} | Decline an authorized order identified by the authorization token
*OrdersApi* | [**getKbbMerchantOrder**](docs/Api/OrdersApi.md#getkbbmerchantorder) | **GET** /v1/authorizations/{authorization_token} | Retrieve an authorized order based on the authorization token

## Models

- [InstantShoppingAddressV1](docs/Model/InstantShoppingAddressV1.md)
- [InstantShoppingAttachmentV1](docs/Model/InstantShoppingAttachmentV1.md)
- [InstantShoppingBaseMerchantUrlsV1](docs/Model/InstantShoppingBaseMerchantUrlsV1.md)
- [InstantShoppingButtonSetupOptionsV1](docs/Model/InstantShoppingButtonSetupOptionsV1.md)
- [InstantShoppingButtonSetupOptionsV1AllOf](docs/Model/InstantShoppingButtonSetupOptionsV1AllOf.md)
- [InstantShoppingCustomerV1](docs/Model/InstantShoppingCustomerV1.md)
- [InstantShoppingErrorResponseV1](docs/Model/InstantShoppingErrorResponseV1.md)
- [InstantShoppingItemV1](docs/Model/InstantShoppingItemV1.md)
- [InstantShoppingLocationAddressV1](docs/Model/InstantShoppingLocationAddressV1.md)
- [InstantShoppingMerchantCheckbox](docs/Model/InstantShoppingMerchantCheckbox.md)
- [InstantShoppingMerchantCreateOrderRequestV1](docs/Model/InstantShoppingMerchantCreateOrderRequestV1.md)
- [InstantShoppingMerchantCreateOrderResponseV1](docs/Model/InstantShoppingMerchantCreateOrderResponseV1.md)
- [InstantShoppingMerchantDeclineOrderRequestV1](docs/Model/InstantShoppingMerchantDeclineOrderRequestV1.md)
- [InstantShoppingMerchantGetOrderResponseV1](docs/Model/InstantShoppingMerchantGetOrderResponseV1.md)
- [InstantShoppingMerchantRequestButtonSetupOptionsV1](docs/Model/InstantShoppingMerchantRequestButtonSetupOptionsV1.md)
- [InstantShoppingMerchantUrlsV1](docs/Model/InstantShoppingMerchantUrlsV1.md)
- [InstantShoppingOptionsV1](docs/Model/InstantShoppingOptionsV1.md)
- [InstantShoppingOrderLineV1](docs/Model/InstantShoppingOrderLineV1.md)
- [InstantShoppingOrderMerchantUrlsV1](docs/Model/InstantShoppingOrderMerchantUrlsV1.md)
- [InstantShoppingProductAttributeV1](docs/Model/InstantShoppingProductAttributeV1.md)
- [InstantShoppingProductIdentifiersV1](docs/Model/InstantShoppingProductIdentifiersV1.md)
- [InstantShoppingSelectedShippingOptionV1](docs/Model/InstantShoppingSelectedShippingOptionV1.md)
- [InstantShoppingSelectedShippingOptionV1AllOf](docs/Model/InstantShoppingSelectedShippingOptionV1AllOf.md)
- [InstantShoppingShippingAttributesV1](docs/Model/InstantShoppingShippingAttributesV1.md)
- [InstantShoppingShippingAttributesV1Dimensions](docs/Model/InstantShoppingShippingAttributesV1Dimensions.md)
- [InstantShoppingShippingDeliveryDetailsV1](docs/Model/InstantShoppingShippingDeliveryDetailsV1.md)
- [InstantShoppingShippingDeliveryDetailsV1CarrierProduct](docs/Model/InstantShoppingShippingDeliveryDetailsV1CarrierProduct.md)
- [InstantShoppingShippingDeliveryDetailsV1Location](docs/Model/InstantShoppingShippingDeliveryDetailsV1Location.md)
- [InstantShoppingShippingDeliveryDetailsV1Timeslot](docs/Model/InstantShoppingShippingDeliveryDetailsV1Timeslot.md)
- [InstantShoppingShippingOptionV1](docs/Model/InstantShoppingShippingOptionV1.md)
- [InstantShoppingStylingOptionsV1](docs/Model/InstantShoppingStylingOptionsV1.md)
- [InstantShoppingStylingOptionsV1Theme](docs/Model/InstantShoppingStylingOptionsV1Theme.md)
- [InstantShoppingTokenizationV1](docs/Model/InstantShoppingTokenizationV1.md)

## Authorization

### basicAuth

- **Type**: HTTP basic authentication

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
