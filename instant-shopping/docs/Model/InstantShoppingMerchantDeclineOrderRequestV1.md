# # InstantShoppingMerchantDeclineOrderRequestV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deny_redirect_url** | **string** | If you specify a redirect_url then we will redirect the consumer to this page and do not show any information dialog within the Instant Shopping flow. | [optional]
**deny_code** | **string** | Acceptable values for code are: &#39;address_error&#39;, &#39;item_out_of_stock&#39;, &#39;consumer_underaged&#39;, &#39;unsupported_shipping_address&#39;. You don&#39;t need to specify a message for these codes. &#39;other&#39; is also an acceptable value for which you may specify a message which will be shown to the consumer. It is important that the language of the message matches the locale of the Instant Shopping. | [optional]
**deny_message** | **string** | A message that will be shown to the consumer when denied | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
