# # InstantShoppingItemV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Descriptive product name. |
**type** | **string** | The type of product variation. Choose between physical or digital. |
**reference** | **string** | Article number, SKU or similar. Max length is 64 characters. | [optional]
**tax_rate** | **int** | Non-negative. In percent, two implicit decimals. I.e 2500 &#x3D; 25%. |
**total_amount** | **int** | Includes tax and discount. Must match (quantity unit_price) - total_discount_amount within ±quantity. (max value: 100000000) | [optional]
**total_tax_amount** | **int** | Must be within ±1 of total_amount - total_amount 10000 / (10000 + tax_rate). Negative when type is discount. |
**total_discount_amount** | **int** | Non-negative minor units. Includes tax. | [optional]
**unit_price** | **int** | Minor units. Includes tax, excludes discount. (max value: 100000000) |
**merchant_data** | **string** | Pass through field. (max 255 characters) | [optional]
**product_url** | **string** | URL to an image that can be later embedded in communications between Klarna and the customer. (max 1024 characters) | [optional]
**image_url** | **string** | URL of the image of the product. Will be used to help consumers selecting between product variations | [optional]
**product_identifiers** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingProductIdentifiersV1**](InstantShoppingProductIdentifiersV1.md) |  | [optional]
**shipping_attributes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingAttributesV1**](InstantShoppingShippingAttributesV1.md) |  | [optional]
**group_identifier** | **string** | Must not include the character &#39;_&#39;. Unique identifier of the product that is described in multiple variations in this &#39;items&#39; list. |
**product_attributes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingProductAttributeV1[]**](InstantShoppingProductAttributeV1.md) | List of objects, each describe every possible product variation as a unique combination of product attributes, e.g. color, size, material. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
