# # InstantShoppingShippingDeliveryDetailsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **string** | The carrier name | [optional]
**location** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingDeliveryDetailsV1Location**](InstantShoppingShippingDeliveryDetailsV1Location.md) |  | [optional]
**timeslot** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingDeliveryDetailsV1Timeslot**](InstantShoppingShippingDeliveryDetailsV1Timeslot.md) |  | [optional]
**carrier_product** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingDeliveryDetailsV1CarrierProduct**](InstantShoppingShippingDeliveryDetailsV1CarrierProduct.md) |  | [optional]
**shipping_class** | **string** | The shipping class | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
