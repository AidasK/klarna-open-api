# # InstantShoppingMerchantRequestButtonSetupOptionsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the button key. | [optional]
**disabled** | **bool** | Controls the visibility of an Instant Shopping button | [optional]
**merchant_urls** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantUrlsV1**](InstantShoppingMerchantUrlsV1.md) |  |
**purchase_currency** | **string** |  | [optional]
**purchase_country** | **string** | ISO 3166 alpha-2 purchase country | [optional]
**billing_countries** | **string[]** | A list of countries (ISO 3166 alpha-2) to specify allowed billing countries. | [optional]
**shipping_countries** | **string[]** | A list of countries (ISO 3166 alpha-2). Default is purchase_country only. | [optional]
**locale** | **string** | RFC 1766 customer&#39;s locale. | [optional]
**merchant_reference1** | **string** | Used for storing merchant&#39;s internal order number or other reference | [optional]
**merchant_reference2** | **string** | Used for storing merchant&#39;s internal order number or other reference. | [optional]
**options** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingOptionsV1**](InstantShoppingOptionsV1.md) |  | [optional]
**merchant_data** | **string** | Pass through field (max 1024 characters). | [optional]
**attachment** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingAttachmentV1**](InstantShoppingAttachmentV1.md) |  | [optional]
**order_lines** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingOrderLineV1[]**](InstantShoppingOrderLineV1.md) | The applicable order lines. Max 10000 | [optional]
**items** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingItemV1[]**](InstantShoppingItemV1.md) | List of elements, each describe every possible product variation as a unique combination of product attributes, e.g. color, size, material. | [optional]
**shipping_options** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingOptionV1[]**](InstantShoppingShippingOptionV1.md) | A list of shipping options available for this order. | [optional]
**shipping_attributes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingAttributesV1**](InstantShoppingShippingAttributesV1.md) |  | [optional]
**styling** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingStylingOptionsV1**](InstantShoppingStylingOptionsV1.md) |  | [optional]
**tokenization** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingTokenizationV1**](InstantShoppingTokenizationV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
