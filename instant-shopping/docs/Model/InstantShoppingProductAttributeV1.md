# # InstantShoppingProductAttributeV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** | Must not include the character &#39;_&#39;. Unique identifier of this attribute. E.g. &#39;clr.22&#39; for a color attribute. |
**identifier_label** | **string** | This is a label that will be presented to the consumer when they will be asked to specify this attribute&#39;s value |
**value** | **string** | Must not include the character &#39;_&#39;. Unique identifier of the value of this product attribute |
**value_label** | **string** | The value presented to the consumer for selection for a specific product attribute |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
