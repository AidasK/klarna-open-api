# # InstantShoppingBaseMerchantUrlsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terms** | **string** | URL of a page on the merchant side describing the terms and conditions. (max 2000 characters) | [optional]
**update** | **string** | URL of an endpoint at the merchant side, which will receive a callback when an order is updated. (must be https, max 2000 characters) | [optional]
**place_order** | **string** | URL of an endpoint at the merchant side, which will receive a ping to place an order. (must be https, max 2000 characters) | [optional]
**create_customer_token** | **string** | URL of an endpoint at the merchant side, which will receive a ping to create a customer token. (must be https, max 2000 characters) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
