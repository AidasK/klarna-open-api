# # InstantShoppingShippingDeliveryDetailsV1Timeslot

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **string** | Identifier of the timeslot | [optional]
**start** | **string** | Timeslot start time | [optional]
**end** | **string** | Timeslot end time | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
