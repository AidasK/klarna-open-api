# # InstantShoppingStylingOptionsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**theme** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingStylingOptionsV1Theme**](InstantShoppingStylingOptionsV1Theme.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
